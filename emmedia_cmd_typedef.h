#pragma once

#define __EMMEDIA_NAME(_index, _name) __LIB2_FNE_NAME_LEFT(__E_FNENAME)##_##_name##_##_index##_

// 传递函数名和索引, 拼接成 定义库名_名字_序号_定义库名, 比如 emmedia_test_0_emmedia
#define EMMEDIA_NAME(_index, _name) __LIB2_FNE_NAME_LEFT(__EMMEDIA_NAME(_index, _name))__LIB2_FNE_NAME_LEFT(__E_FNENAME)

// 传递函数名和索引, 拼接成 "定义库名_名字_序号_定义库名", 比如 "emmedia_test_0_emmedia"
#define EMMEDIA_NAME_STR(_index, _name) ______E_FNENAME(__EMMEDIA_NAME(_index, _name))

// 这个宏定义了所有的命令, 以后需要命令名数组, 声明命令等, 都可以使用这个宏
#define EMMEDIA_DEF(_MAKE) \
    _MAKE(  0, "打开", Open, "同时打开系统的一个音量类型的音量控制和静音控制，返回值为下列之一：1、#全部打开成功；0、全部打开失败；-1、音量控制打开失败；-2、#静音打开失败", -1, _CMD_OS(__OS_WIN), SDT_INT, 0, LVL_SIMPLE, 0, 0, 1, g_argumentInfo_emmedia_global_var + 0)\
    _MAKE(  1, "是否打开", IsOpen, "判断音量控制或静音控制是否打开，如打开返回真,否则返回假", -1, _CMD_OS(__OS_WIN), SDT_BOOL, 0, LVL_SIMPLE, 0, 0, 1, g_argumentInfo_emmedia_global_var + 1)\
    _MAKE(  2, "关闭", Close, "关闭音量控制或静音控制", -1, _CMD_OS(__OS_WIN), SDT_BOOL, 0, LVL_SIMPLE, 0, 0, 1, g_argumentInfo_emmedia_global_var + 1)\
    _MAKE(  3, "取静音", GetMute, "如果当前处于静音状态返回真，否则返回假", -1, _CMD_OS(__OS_WIN), SDT_BOOL, 0, LVL_SIMPLE, 0, 0, 0, g_argumentInfo_emmedia_global_var + 0)\
    _MAKE(  4, "置静音", SetMute, "设置是否静音。成功返回真，失败返回假", -1, _CMD_OS(__OS_WIN), SDT_BOOL, 0, LVL_SIMPLE, 0, 0, 1, g_argumentInfo_emmedia_global_var + 2)\
    _MAKE(  5, "取最大音量", GetVolumeMax, "获取当前音量控制可能被调整到的最大值", -1, _CMD_OS(__OS_WIN), SDT_INT, 0, LVL_SIMPLE, 0, 0, 0, g_argumentInfo_emmedia_global_var + 0)\
    _MAKE(  6, "取最小音量", GetVolumeMin, "获取当前音量控制可能被调整到的最小值", -1, _CMD_OS(__OS_WIN), SDT_INT, 0, LVL_SIMPLE, 0, 0, 0, g_argumentInfo_emmedia_global_var + 0)\
    _MAKE(  7, "取音量", GetVolume, "获取左声道和右声道的音量，成功返回真，失败返回假", -1, _CMD_OS(__OS_WIN), SDT_BOOL, 0, LVL_SIMPLE, 0, 0, 2, g_argumentInfo_emmedia_global_var + 3)\
    _MAKE(  8, "置音量", SetVolume, "设置左声道和右声道的音量。成功返回真，失败返回假", -1, _CMD_OS(__OS_WIN), SDT_BOOL, 0, LVL_SIMPLE, 0, 0, 2, g_argumentInfo_emmedia_global_var + 5)\
    _MAKE(  9, "取设备号", GetDeviceID, "获取音量控制或静音控制的标识号", -1, _CMD_OS(__OS_WIN), SDT_INT, 0, LVL_SIMPLE, 0, 0, 1, g_argumentInfo_emmedia_global_var + 7)\
    _MAKE( 10, "取设备名", GetDeviceName, "获取设备的名称", -1, _CMD_OS(__OS_WIN), SDT_TEXT, 0, LVL_SIMPLE, 0, 0, 1, g_argumentInfo_emmedia_global_var + 7)\
    _MAKE( 11, "录制", Record, "开始录制。成功返回真，失败返回假", -1, _CMD_OS(__OS_WIN), SDT_BOOL, 0, LVL_SIMPLE, 0, 0, 0, g_argumentInfo_emmedia_global_var + 0)\
    _MAKE( 12, "是否在录制", IsRecording, "判断是否正在录制。成功返回真，失败返回假", -1, _CMD_OS(__OS_WIN), SDT_BOOL, 0, LVL_SIMPLE, 0, 0, 0, g_argumentInfo_emmedia_global_var + 0)\
    _MAKE( 13, "暂停", Pause, "暂停录制。成功返回真，失败返回假", -1, _CMD_OS(__OS_WIN), SDT_BOOL, 0, LVL_SIMPLE, 0, 0, 0, g_argumentInfo_emmedia_global_var + 0)\
    _MAKE( 14, "继续", Continue, "恢复一个被暂停的录制。成功返回真，失败返回假", -1, _CMD_OS(__OS_WIN), SDT_BOOL, 0, LVL_SIMPLE, 0, 0, 0, g_argumentInfo_emmedia_global_var + 0)\
    _MAKE( 15, "停止", Stop, "停止录制。成功返回真，失败返回假", -1, _CMD_OS(__OS_WIN), SDT_BOOL, 0, LVL_SIMPLE, 0, 0, 0, g_argumentInfo_emmedia_global_var + 0)\
    _MAKE( 16, "置格式", SetWaveFormat, "设置录音的格式，注意参数的正确性，成功返回真，失败返回假。注意：本方法必须在使用\"录音\"方法前使用", -1, _CMD_OS(__OS_WIN), SDT_BOOL, 0, LVL_SIMPLE, 0, 0, 3, g_argumentInfo_emmedia_global_var + 8)\
    _MAKE( 17, "取格式", GetWaveFormat, "获取录音格式", -1, _CMD_OS(__OS_WIN), SDT_BOOL, 0, LVL_SIMPLE, 0, 0, 3, g_argumentInfo_emmedia_global_var + 11)\
    _MAKE( 18, "保存文件", SaveFile, "以*.wav格式保存当前录制的声音到指定文件中，成功返回真，失败返回假", -1, _CMD_OS(__OS_WIN), SDT_BOOL, 0, LVL_SIMPLE, 0, 0, 2, g_argumentInfo_emmedia_global_var + 14)\
    _MAKE( 19, "取位置", GetPosition, "返回当前已录制的字节的(Bytes)位置, 失败返回-1", -1, _CMD_OS(__OS_WIN), SDT_INT, 0, LVL_SIMPLE, 0, 0, 0, g_argumentInfo_emmedia_global_var + 0)\
    _MAKE( 20, "取设备数量", GetDeviceNum, "获取当前可以使用MIDI设备的数量", -1, _CMD_OS(__OS_WIN), SDT_INT, 0, LVL_SIMPLE, 0, 0, 0, g_argumentInfo_emmedia_global_var + 0)\
    _MAKE( 21, "取设备名", GetDeviceName, "获取指定MIDI设备的名称", -1, _CMD_OS(__OS_WIN), SDT_TEXT, 0, LVL_SIMPLE, 0, 0, 1, g_argumentInfo_emmedia_global_var + 16)\
    _MAKE( 22, "打开", Open, "通过MIDI设备序号打开指定的MIDI设备，成功返回真，失败返回假", -1, _CMD_OS(__OS_WIN), SDT_BOOL, 0, LVL_SIMPLE, 0, 0, 1, g_argumentInfo_emmedia_global_var + 16)\
    _MAKE( 23, "是否打开", IsOpen, "判断MIDI设备是否打开，如打开返回真，否则返回假", -1, _CMD_OS(__OS_WIN), SDT_BOOL, 0, LVL_SIMPLE, 0, 0, 0, g_argumentInfo_emmedia_global_var + 0)\
    _MAKE( 24, "关闭", Close, "关闭当前MIDI设备，成功返回真，否则返回假", -1, _CMD_OS(__OS_WIN), SDT_BOOL, 0, LVL_SIMPLE, 0, 0, 0, g_argumentInfo_emmedia_global_var + 0)\
    _MAKE( 25, "置乐器", SetInstrument, "以指定的乐器序号设置当前MIDI设置要演奏的乐器，成功返回真，失败返回假", -1, _CMD_OS(__OS_WIN), SDT_BOOL, 0, LVL_SIMPLE, 0, 0, 2, g_argumentInfo_emmedia_global_var + 17)\
    _MAKE( 26, "取乐器序号", GetInstrument, "获取指定通道当前使用的乐器序号，乐器序号从1开始", -1, _CMD_OS(__OS_WIN), SDT_INT, 0, LVL_SIMPLE, 0, 0, 1, g_argumentInfo_emmedia_global_var + 19)\
    _MAKE( 27, "取乐器数量", GetInstrumentNum, "获取当前所有支持的乐器的数量", -1, _CMD_OS(__OS_WIN), SDT_INT, 0, LVL_SIMPLE, 0, 0, 0, g_argumentInfo_emmedia_global_var + 0)\
    _MAKE( 28, "取乐器名", GetInstrumentName, "通过指定乐器序号获取乐器名称", -1, _CMD_OS(__OS_WIN), SDT_TEXT, 0, LVL_SIMPLE, 0, 0, 1, g_argumentInfo_emmedia_global_var + 17)\
    _MAKE( 29, "奏", NoteOn, "演奏一个音符。成功返回真，失败返回假", -1, _CMD_OS(__OS_WIN), SDT_BOOL, 0, LVL_SIMPLE, 0, 0, 2, g_argumentInfo_emmedia_global_var + 20)\
    _MAKE( 30, "停奏", NoteOff, "停止演奏一个音符。成功返回真，失败返回假", -1, _CMD_OS(__OS_WIN), SDT_BOOL, 0, LVL_SIMPLE, 0, 0, 2, g_argumentInfo_emmedia_global_var + 22)\
    _MAKE( 31, "打开", Open, "打开设备接收声波输入，成功返回真，失败返回假", -1, _CMD_OS(__OS_WIN), SDT_BOOL, 0, LVL_SIMPLE, 0, 0, 0, g_argumentInfo_emmedia_global_var + 0)\
    _MAKE( 32, "是否打开", IsOpen, "判断设备是否打开，如打开返回真，否则返回假", -1, _CMD_OS(__OS_WIN), SDT_BOOL, 0, LVL_SIMPLE, 0, 0, 0, g_argumentInfo_emmedia_global_var + 0)\
    _MAKE( 33, "关闭", Close, "关闭已打开设备，成功返回真，失败返回假", -1, _CMD_OS(__OS_WIN), SDT_BOOL, 0, LVL_SIMPLE, 0, 0, 0, g_argumentInfo_emmedia_global_var + 0)\
    _MAKE( 34, "置间隔", SetWaveInInterval, NULL, -1, _CMD_OS(__OS_WIN) | CT_IS_HIDED, SDT_BOOL, 0, LVL_SIMPLE, 0, 0, 1, g_argumentInfo_emmedia_global_var + 24)\
    _MAKE( 35, "取间隔", GetWaveInInterval, NULL, -1, _CMD_OS(__OS_WIN) | CT_IS_HIDED, SDT_INT, 0, LVL_SIMPLE, 0, 0, 0, g_argumentInfo_emmedia_global_var + 0)\
    _MAKE( 36, "取上限", GetUpBound, "取可以接收的声波值的最大频段（也可以理解为数组上限）", -1, _CMD_OS(__OS_WIN), SDT_INT, 0, LVL_SIMPLE, 0, 0, 0, g_argumentInfo_emmedia_global_var + 0)\
    _MAKE( 37, "取声波值", GetWaveValue, "获取指定频段的声波值。成功返回真，失败返回假", -1, _CMD_OS(__OS_WIN), SDT_BOOL, 0, LVL_SIMPLE, 0, 0, 2, g_argumentInfo_emmedia_global_var + 25)\
    _MAKE( 38, "打开", Open, "打开指定的光驱。成功返回真，失败返回假", -1, _CMD_OS(__OS_WIN), SDT_BOOL, 0, LVL_SIMPLE, 0, 0, 1, g_argumentInfo_emmedia_global_var + 27)\
    _MAKE( 39, "关闭", Close, "关闭光驱。成功返回真，失败返回假", -1, _CMD_OS(__OS_WIN), SDT_BOOL, 0, LVL_SIMPLE, 0, 0, 0, g_argumentInfo_emmedia_global_var + 0)\
    _MAKE( 40, "开关仓门", DoorPower, "打开/关闭光驱仓门。成功返回真，失败返回假", -1, _CMD_OS(__OS_WIN) | CT_IS_HIDED, SDT_BOOL, 0, LVL_SIMPLE, 0, 0, 0, g_argumentInfo_emmedia_global_var + 0)\
    _MAKE( 41, "开仓门", DoorOpen, "打开光驱仓门。成功返回真，失败返回假", -1, _CMD_OS(__OS_WIN), SDT_BOOL, 0, LVL_SIMPLE, 0, 0, 0, g_argumentInfo_emmedia_global_var + 0)\
    _MAKE( 42, "关仓门", DoorClose, "关闭光驱仓门。成功返回真，失败返回假", -1, _CMD_OS(__OS_WIN), SDT_BOOL, 0, LVL_SIMPLE, 0, 0, 0, g_argumentInfo_emmedia_global_var + 0)\
    _MAKE( 43, "是否有CD", HasCD, "判断当前光驱中是否有光盘，如果有返回真，否则返回假", -1, _CMD_OS(__OS_WIN), SDT_BOOL, 0, LVL_SIMPLE, 0, 0, 0, g_argumentInfo_emmedia_global_var + 0)\
    _MAKE( 44, "取状态", GetMode, "返回当前光驱的状态，为以下常量值之一：0、#未知状态；1、#播放状态；2、#停止状态；3、#弹出状态和无光盘状态", -1, _CMD_OS(__OS_WIN), SDT_INT, 0, LVL_SIMPLE, 0, 0, 0, g_argumentInfo_emmedia_global_var + 0)\
    _MAKE( 45, "播放", Play, "开始播放，成功返回真，失败返回假", -1, _CMD_OS(__OS_WIN), SDT_BOOL, 0, LVL_SIMPLE, 0, 0, 0, g_argumentInfo_emmedia_global_var + 0)\
    _MAKE( 46, "暂停", Pause, "暂停播放，成功返回真，失败返回假", -1, _CMD_OS(__OS_WIN), SDT_BOOL, 0, LVL_SIMPLE, 0, 0, 0, g_argumentInfo_emmedia_global_var + 0)\
    _MAKE( 47, "恢复", Resume, "恢复播放，成功返回真，失败返回假", -1, _CMD_OS(__OS_WIN), SDT_BOOL, 0, LVL_SIMPLE, 0, 0, 0, g_argumentInfo_emmedia_global_var + 0)\
    _MAKE( 48, "停止", Stop, "停止播放，成功返回真，失败返回假", -1, _CMD_OS(__OS_WIN), SDT_BOOL, 0, LVL_SIMPLE, 0, 0, 0, g_argumentInfo_emmedia_global_var + 0)\
    _MAKE( 49, "跳到", JumpTo, "跳到指定曲目的指定位置。成功返回真，失败返回假", -1, _CMD_OS(__OS_WIN), SDT_BOOL, 0, LVL_SIMPLE, 0, 0, 1, g_argumentInfo_emmedia_global_var + 28)\
    _MAKE( 50, "移到", SeekTo, "将播放位置移动到当前光盘的指定位置。成功返回真，失败返回假", -1, _CMD_OS(__OS_WIN) | CT_IS_HIDED, SDT_BOOL, 0, LVL_SIMPLE, 0, 0, 1, g_argumentInfo_emmedia_global_var + 29)\
    _MAKE( 51, "取位置", GetPosition, "获取当前的播放位置，成功返回真，失败返回假", -1, _CMD_OS(__OS_WIN), SDT_BOOL, 0, LVL_SIMPLE, 0, 0, 2, g_argumentInfo_emmedia_global_var + 30)\
    _MAKE( 52, "取曲目数", GetCount, "获取当前CD光盘中的曲目数量", -1, _CMD_OS(__OS_WIN), SDT_INT, 0, LVL_SIMPLE, 0, 0, 0, g_argumentInfo_emmedia_global_var + 0)\
    _MAKE( 53, "取长度", GetLength, "获取指定曲目的时间长度，以毫秒为单位", -1, _CMD_OS(__OS_WIN), SDT_INT, 0, LVL_SIMPLE, 0, 0, 1, g_argumentInfo_emmedia_global_var + 32)\
    _MAKE( 54, "取总长度", GetFullLength, "获取全部曲目的时间长度的总和，以毫秒为单位", -1, _CMD_OS(__OS_WIN), SDT_INT, 0, LVL_SIMPLE, 0, 0, 0, g_argumentInfo_emmedia_global_var + 0)\
    _MAKE( 55, "取别名", GetMCIAlias, "获取当前对象使用的MCI控制别名", -1, _CMD_OS(__OS_WIN), SDT_TEXT, 0, LVL_SIMPLE, 0, 0, 0, g_argumentInfo_emmedia_global_var + 0)\
    _MAKE( 56, "打开", Open, "打开指定媒体文件。成功返回真，失败返回假", -1, _CMD_OS(__OS_WIN), SDT_BOOL, 0, LVL_SIMPLE, 0, 0, 1, g_argumentInfo_emmedia_global_var + 33)\
    _MAKE( 57, "关闭", Close, "关闭当前播放的媒体。成功返回真，失败返回假", -1, _CMD_OS(__OS_WIN), SDT_BOOL, 0, LVL_SIMPLE, 0, 0, 0, g_argumentInfo_emmedia_global_var + 0)\
    _MAKE( 58, "是否视频", IsVideo, "判断当前播放的内容是否是视频。如果是返回真，否则返回假", -1, _CMD_OS(__OS_WIN), SDT_BOOL, 0, LVL_SIMPLE, 0, 0, 0, g_argumentInfo_emmedia_global_var + 0)\
    _MAKE( 59, "取句柄", GetHwnd, "获取当前视频播放窗口的Windows 窗口句柄", -1, _CMD_OS(__OS_WIN), SDT_INT, 0, LVL_SIMPLE, 0, 0, 0, g_argumentInfo_emmedia_global_var + 0)\
    _MAKE( 60, "置句柄", SetHwnd, "设置当前视频播放窗口的Windows 窗口句柄，成功返回真，失败返回假", -1, _CMD_OS(__OS_WIN), SDT_BOOL, 0, LVL_SIMPLE, 0, 0, 1, g_argumentInfo_emmedia_global_var + 34)\
    _MAKE( 61, "取状态", GetMode, "获取当前的播放状态，为以下常量值之一：0、#未知状态；1、#播放状态；2、#停止状态；3、#暂停状态", -1, _CMD_OS(__OS_WIN), SDT_INT, 0, LVL_SIMPLE, 0, 0, 0, g_argumentInfo_emmedia_global_var + 0)\
    _MAKE( 62, "取长度", GetLength, "获取当前媒体文件的长度", -1, _CMD_OS(__OS_WIN), SDT_INT, 0, LVL_SIMPLE, 0, 0, 0, g_argumentInfo_emmedia_global_var + 0)\
    _MAKE( 63, "取位置", GetPosition, "获取当前的播放位置", -1, _CMD_OS(__OS_WIN), SDT_INT, 0, LVL_SIMPLE, 0, 0, 0, g_argumentInfo_emmedia_global_var + 0)\
    _MAKE( 64, "取总时间", GetTotalSec, "以秒为单位获取总播放时间", -1, _CMD_OS(__OS_WIN), SDT_INT, 0, LVL_SIMPLE, 0, 0, 0, g_argumentInfo_emmedia_global_var + 0)\
    _MAKE( 65, "取比率", GetFrameRate, "获取每一帧的比率", -1, _CMD_OS(__OS_WIN), SDT_INT, 0, LVL_SIMPLE, 0, 0, 0, g_argumentInfo_emmedia_global_var + 0)\
    _MAKE( 66, "播放", Play, "从指定位置开始播放。成功返回真，失败返回假", -1, _CMD_OS(__OS_WIN), SDT_BOOL, 0, LVL_SIMPLE, 0, 0, 2, g_argumentInfo_emmedia_global_var + 35)\
    _MAKE( 67, "暂停", Pause, "暂停播放。成功返回真，失败返回假", -1, _CMD_OS(__OS_WIN), SDT_BOOL, 0, LVL_SIMPLE, 0, 0, 0, g_argumentInfo_emmedia_global_var + 0)\
    _MAKE( 68, "停止", Stop, "停止播放。成功返回真，失败返回假", -1, _CMD_OS(__OS_WIN), SDT_BOOL, 0, LVL_SIMPLE, 0, 0, 0, g_argumentInfo_emmedia_global_var + 0)\
    _MAKE( 69, "取别名", GetMCIAlias, "获取当前对象使用的MCI别名", -1, _CMD_OS(__OS_WIN), SDT_TEXT, 0, LVL_SIMPLE, 0, 0, 0, g_argumentInfo_emmedia_global_var + 0)\
    _MAKE( 70, "取起始时间", GetTrackBeginTime, "获取指定曲目的起始时间", -1, _CMD_OS(__OS_WIN), SDT_INT, 0, LVL_SIMPLE, 0, 0, 1, g_argumentInfo_emmedia_global_var + 37)\
    _MAKE( 71, "初始化", Init, "初始化音频转换。成功返回真，失败返回假。在使用本对象前，必须保证成功调用过本方法", -1, _CMD_OS(__OS_WIN), SDT_BOOL, 0, LVL_SIMPLE, 0, 0, 0, g_argumentInfo_emmedia_global_var + 0)\
    _MAKE( 72, "取编码数", GetCodeCount, "获取当前编码器可用的编码格式数量。失败返回0", -1, _CMD_OS(__OS_WIN), SDT_INT, 0, LVL_SIMPLE, 0, 0, 0, g_argumentInfo_emmedia_global_var + 0)\
    _MAKE( 73, "取编码信息", GetCodeInfo, "根据当前编码器的指定编码格式索引获取编码格式的相关信息。成功返回真，失败返回假", -1, _CMD_OS(__OS_WIN), SDT_BOOL, 0, LVL_SIMPLE, 0, 0, 4, g_argumentInfo_emmedia_global_var + 38)\
    _MAKE( 74, "转换", Convert, "转换音频文件。成功返回真，失败返回假", -1, _CMD_OS(__OS_WIN), SDT_BOOL, 0, LVL_SIMPLE, 0, 0, 3, g_argumentInfo_emmedia_global_var + 42)\
    _MAKE( 75, "取编码器数", GetEncoderCount, "获取当前可用的编码器的数量。失败返回0", -1, _CMD_OS(__OS_WIN), SDT_INT, 0, LVL_SIMPLE, 0, 0, 0, g_argumentInfo_emmedia_global_var + 0)\
    _MAKE( 76, "取编码器名", GetEncoderName, "获取指定编码器索引对应的编码器名称。失败返回空文本", -1, _CMD_OS(__OS_WIN), SDT_TEXT, 0, LVL_SIMPLE, 0, 0, 1, g_argumentInfo_emmedia_global_var + 45)\
    _MAKE( 77, "取当前编码器", GetCurrentEncoder, "获取当前使用的编码器的名称。", -1, _CMD_OS(__OS_WIN), SDT_TEXT, 0, LVL_SIMPLE, 0, 0, 0, g_argumentInfo_emmedia_global_var + 0)\
    _MAKE( 78, "置当前编码器", SetCurrentEncoder, "设置当前使用的编码器的名称。成功返回真，失败返回假", -1, _CMD_OS(__OS_WIN), SDT_BOOL, 0, LVL_SIMPLE, 0, 0, 1, g_argumentInfo_emmedia_global_var + 46)\
    _MAKE( 79, "置音量", SetVolume, "设置当前播放媒体的音量，不影响系统音量。左右声道中只要有一个设置失败，就会返回假。", -1, _CMD_OS(__OS_WIN), SDT_BOOL, 0, LVL_SIMPLE, 0, 0, 2, g_argumentInfo_emmedia_global_var + 47)\
    _MAKE( 80, "取音量", GetVolume, "获取当前播放音量（非系统音量）。左右声道中只要有一个获取失败，就会返回假。", -1, _CMD_OS(__OS_WIN), SDT_BOOL, 0, LVL_SIMPLE, 0, 0, 2, g_argumentInfo_emmedia_global_var + 49)

