#include "include_emmedia_header.h"

//TODO 静态库需要的部分,会记录所有的函数名到数组里,静态编译的时候需要取回命令名
#if !defined(__E_STATIC_LIB)
static ARG_INFO g_argumentInfo_emmedia_global_var[] = 
{
    // 1=参数名称, 2=参数详细解释, 3=指定图像索引,从1开始,0表示无, 4=图像数目(用作动画)
    // 5=参数类型, 6=参数默认值,   7=参数标志 AS_ 开头常量
    // AS_HAS_DEFAULT_VALUE         有默认值,倒数第二个参数是默认值
    // AS_DEFAULT_VALUE_IS_EMPTY    默认值为空,有可空标志
    // AS_RECEIVE_VAR               只能传递变量,相当于传引用,传递过来的肯定不是数组
    // AS_RECEIVE_VAR_ARRAY         传递过来的肯定是数组变量引用
    // AS_RECEIVE_VAR_OR_ARRAY      传递变量或者数组变量引用
    // AS_RECEIVE_ARRAY_DATA        传递数组
    // AS_RECEIVE_ALL_TYPE_DATA     传递数组/非数组
    // AS_RECEIVE_VAR_OR_OTHER      可以传递 变量/数据/返回值数据

    /*000*/ {"设备标识", "该参数必须提供合法的设备标识，标识只能为本支持库的\"音量类型\"下的常量之一", 0, 0, SDT_INT, 0, NULL},

    /*001*/ {"类型", "0、#音量；1、#静音； 其他值、#全部", 0, 0, SDT_INT, 2, AS_HAS_DEFAULT_VALUE},

    /*002*/ {"静音", "该参数设定是否静音", 0, 0, SDT_BOOL, 0, NULL},

    /*003*/ {"左声道音量", "该参数接收左声道的音量", 0, 0, SDT_INT, 0, AS_RECEIVE_VAR},
    /*004*/ {"右声道音量", "该参数接收右声道的音量", 0, 0, SDT_INT, 0, AS_RECEIVE_VAR},

    /*005*/ {"左声道音量", "要设置的左声道的音量", 0, 0, SDT_INT, 0, NULL},
    /*006*/ {"右声道音量", "要设置的右声道的音量", 0, 0, SDT_INT, 0, NULL},

    /*007*/ {"设备标识", "0表示音量，非0表示静音", 0, 0, SDT_INT, 0, AS_HAS_DEFAULT_VALUE},

    /*008*/ {"线路数", "本参数为录音的线路数，为以下常量值之一：1、#单声道；2、立体声双声道", 0, 0, SDT_INT, 1, AS_HAS_DEFAULT_VALUE},
    /*009*/ {"采样率", "本参数为每秒采样率(hertz)，为以下常量值之一：8000、#8000HZ；11025、#11025HZ；22050、#22050HZ；44100、#44100HZ", 0, 0, SDT_INT, 8000, AS_HAS_DEFAULT_VALUE},
    /*010*/ {"采样大小", "本参数为采样大小，以位为单位，为以下常量值之一：8、#8位；16、#16位", 0, 0, SDT_INT, 16, AS_HAS_DEFAULT_VALUE},

    /*011*/ {"线路数", "本参数为录音的线路数，为以下常量值之一：1、#单声道；2、立体声双声道", 0, 0, SDT_INT, 0, AS_RECEIVE_VAR},
    /*012*/ {"采样率", "本参数为每秒采样率(hertz)，为以下常量值之一：11025、#11025HZ；22050、#22050HZ；44100、#44100HZ", 0, 0, SDT_INT, 0, AS_RECEIVE_VAR},
    /*013*/ {"采样大小", "本参数为采样大小，以位为单位，为以下常量值之一：8、#8位；16、#16位", 0, 0, SDT_INT, 0, AS_RECEIVE_VAR},

    /*014*/ {"保存文件名", NULL, 0, 0, SDT_TEXT, 0, NULL},
    /*015*/ {"覆盖已有文件", NULL, 0, 0, SDT_BOOL, 1, AS_HAS_DEFAULT_VALUE},

    /*016*/ {"设备序号", "设备序号从0至最大设备数减1，最大设备数可以用\"取设备数量\"命令获取", 0, 0, SDT_INT, 0, NULL},

    /*017*/ {"乐器序号", "乐器序号范围为1至最大支持乐器数，最大支持乐器数可以通过\"取乐器数量\"方法取得", 0, 0, SDT_INT, 0, NULL},
    /*018*/ {"通道", "该参数指定设置乐器序号的通道，从0 ~ 15", 0, 0, SDT_INT, 0, AS_DEFAULT_VALUE_IS_EMPTY},

    /*019*/ {"通道", "该参数指定乐器序号的通道，从0 ~ 15", 0, 0, SDT_INT, 0, AS_DEFAULT_VALUE_IS_EMPTY},

    /*020*/ {"音符", "该参数范围为0至127。0表示音C，1表示C#，依次类推，127表示第10个8度的127", 0, 0, SDT_BYTE, 0, NULL},
    /*021*/ {"通道", "该参数指定音符演奏的通道，从0 ~ 15", 0, 0, SDT_INT, 0, AS_DEFAULT_VALUE_IS_EMPTY},

    /*022*/ {"音符", "该参数范围为0至127。0表示音C，1表示C#，依次类推，127表示第10个8度的127", 0, 0, SDT_BYTE, 0, NULL},
    /*023*/ {"通道", "该参数指定音符停止演奏的通道，从0 ~ 15", 0, 0, SDT_INT, 0, AS_DEFAULT_VALUE_IS_EMPTY},

    /*024*/ {"间隔", NULL, 0, 0, SDT_INT, 0, NULL},

    /*025*/ {"声波值", "接收声波值的变量", 0, 0, SDT_SHORT, 0, AS_RECEIVE_VAR},
    /*026*/ {"复制序号", "要声波值的序号，序号的范围为0至最大频段减1，最大频段可以通过\"取上限\"方法获取", 0, 0, SDT_INT, 0, NULL},

    /*027*/ {"光驱盘符", "该参数指定要控制的光驱的盘符，必须为如下格式: \"H:\\\"，以下格式是非法的： \"H\", \"H:\"；如果提供空文本，表示打开默认第一个光驱", 0, 0, SDT_TEXT, 0, NULL},

    /*028*/ {"曲目号", "该参数为要转到的曲目号", 0, 0, SDT_INT, 0, NULL},

    /*029*/ {"跳转位置", "该位置可以为CD上的任意一个位置，以毫秒为单位", 0, 0, SDT_INT, 0, NULL},

    /*030*/ {"曲目号", "该参数接收曲目号", 0, 0, SDT_INT, 0, AS_RECEIVE_VAR},
    /*031*/ {"位置", "该参数为当前播放在相应曲目中的位置，以毫秒为单位", 0, 0, SDT_INT, 0, AS_RECEIVE_VAR},

    /*032*/ {"曲目号", "该参数为要取长度的曲目号", 0, 0, SDT_INT, 0, NULL},

    /*033*/ {"文件名", "该参数为要打开的媒体文件名", 0, 0, SDT_TEXT, 0, NULL},

    /*034*/ {"窗口句柄", "要设置的窗口句柄", 0, 0, SDT_INT, 0, NULL},

    /*035*/ {"播放位置", "该参数为播放位置，以毫秒为单位。默认为-1，表示从当前处播放", 0, 0, SDT_INT, -1, AS_HAS_DEFAULT_VALUE},
    /*036*/ {"播放窗口", "指定播放窗口的窗口句柄。如果未指定此参数，或句柄非法，则自动使用之前“置句柄”指定的窗口句柄。", 0, 0, SDT_INT, -1, AS_DEFAULT_VALUE_IS_EMPTY},

    /*037*/ {"曲目号", "该参数为曲目的序号", 0, 0, SDT_INT, -1, AS_HAS_DEFAULT_VALUE},

    /*038*/ {"编码索引", "编码格式的索引，从0 ~ 编码数量 - 1", 0, 0, SDT_INT, 0, NULL},
    /*039*/ {"是否为单声道", "判断是否为单声道，如不是则是双声道", 0, 0, SDT_BOOL, 0, AS_RECEIVE_VAR},
    /*040*/ {"采样频率", "每秒的采样频率，以kHz为单位", 0, 0, SDT_INT, 0, AS_RECEIVE_VAR},
    /*041*/ {"音频大小", "每秒音频的大小，以kbps为单位", 0, 0, SDT_INT, 0, AS_RECEIVE_VAR},

    /*042*/ {"源文件名", "要转换的源文件文件名", 0, 0, SDT_TEXT, 0, NULL},
    /*043*/ {"目标文件名", "要转换的目的文件文件名，先前的文件将被复写", 0, 0, SDT_TEXT, 0, NULL},
    /*044*/ {"编码索引", "编码格式的索引，从0 ~ 编码数量 - 1", 0, 0, SDT_INT, 0, NULL},

    /*045*/ {"编码器索引", "编码器索引从0 ~ 编码器数量 -1", 0, 0, SDT_INT, 0, NULL},

    /*046*/ {"编码器名称", "编码器的名称。", 0, 0, SDT_TEXT, 0, NULL},

    /*047*/ {"左声道音量", "范围为0-100，0为静音，100为最大音量。如果本参数被省略或超出范围，则不修改此声道音量。", 0, 0, SDT_INT, -1, AS_DEFAULT_VALUE_IS_EMPTY},
    /*048*/ {"右声道音量", "范围为0-100，0为静音，100为最大音量。如果本参数被省略或超出范围，则不修改此声道音量。", 0, 0, SDT_INT, -1, AS_DEFAULT_VALUE_IS_EMPTY},

    /*049*/ {"左声道音量", "范围为0-100，0为静音，100为最大音量。如果本参数被省略，则不返回此声道音量。", 0, 0, SDT_INT, -1, AS_DEFAULT_VALUE_IS_EMPTY | AS_RECEIVE_VAR},
    /*050*/ {"右声道音量", "范围为0-100，0为静音，100为最大音量。如果本参数被省略，则不返回此声道音量。", 0, 0, SDT_INT, -1, AS_DEFAULT_VALUE_IS_EMPTY | AS_RECEIVE_VAR},

};

#ifdef _DEBUG    // 这里是为了确认参数序号是否正确, 成员数比最后一个序号大1就是正确
const int dbg_cmd_arg_count__ = sizeof(g_argumentInfo_emmedia_global_var) / sizeof(g_argumentInfo_emmedia_global_var[0]);
#endif


#define EMMEDIA_DEF_CMDINFO(_index, _szName, _szEgName, _szExplain, _shtCategory, _wState, _dtRetValType, _wReserved, _shtUserLevel, _shtBitmapIndex, _shtBitmapCount, _nArgCount, _pBeginArgInfo) \
    { _szName, ______E_FNENAME(_szEgName), _szExplain, _shtCategory, _wState, _dtRetValType, _wReserved, _shtUserLevel, _shtBitmapIndex, _shtBitmapCount, _nArgCount, _pBeginArgInfo },


// 易语言每个命令的定义, 会把信息显示在支持库列表里, 这里每个成员都是详细的描述一个命令的信息
CMD_INFO g_cmdInfo_emmedia_global_var[] = 
{
    EMMEDIA_DEF(EMMEDIA_DEF_CMDINFO)
};

int g_cmdInfo_emmedia_global_var_count = sizeof(g_cmdInfo_emmedia_global_var) / sizeof(g_cmdInfo_emmedia_global_var[0]);

#endif

