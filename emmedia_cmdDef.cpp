#include "include_emmedia_header.h"

// 调用格式: SDT_INT (系统音量).打开, 命令说明: "同时打开系统的一个音量类型的音量控制和静音控制，返回值为下列之一：1、#全部打开成功；0、全部打开失败；-1、音量控制打开失败；-2、#静音打开失败"
// 参数<1>: 设备标识 SDT_INT, 参数说明: "该参数必须提供合法的设备标识，标识只能为本支持库的\"音量类型\"下的常量之一"
EMMEDIA_EXTERN_C void emmedia_Open_0_emmedia(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{
    INT      arg1 = pArgInf[1].m_int;

}

// 调用格式: SDT_BOOL (系统音量).是否打开, 命令说明: "判断音量控制或静音控制是否打开，如打开返回真,否则返回假"
// 参数<1>: 类型 SDT_INT, 参数说明: "0、#音量；1、#静音； 其他值、#全部"
EMMEDIA_EXTERN_C void emmedia_IsOpen_1_emmedia(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{
    INT      arg1 = pArgInf[1].m_int;

}

// 调用格式: SDT_BOOL (系统音量).关闭, 命令说明: "关闭音量控制或静音控制"
// 参数<1>: 类型 SDT_INT, 参数说明: "0、#音量；1、#静音； 其他值、#全部"
EMMEDIA_EXTERN_C void emmedia_Close_2_emmedia(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{
    INT      arg1 = pArgInf[1].m_int;

}

// 调用格式: SDT_BOOL (系统音量).取静音, 命令说明: "如果当前处于静音状态返回真，否则返回假"
// 无参数
EMMEDIA_EXTERN_C void emmedia_GetMute_3_emmedia(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{

}

// 调用格式: SDT_BOOL (系统音量).置静音, 命令说明: "设置是否静音。成功返回真，失败返回假"
// 参数<1>: 静音 SDT_BOOL, 参数说明: "该参数设定是否静音"
EMMEDIA_EXTERN_C void emmedia_SetMute_4_emmedia(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{
    BOOL     arg1 = pArgInf[1].m_bool;

}

// 调用格式: SDT_INT (系统音量).取最大音量, 命令说明: "获取当前音量控制可能被调整到的最大值"
// 无参数
EMMEDIA_EXTERN_C void emmedia_GetVolumeMax_5_emmedia(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{

}

// 调用格式: SDT_INT (系统音量).取最小音量, 命令说明: "获取当前音量控制可能被调整到的最小值"
// 无参数
EMMEDIA_EXTERN_C void emmedia_GetVolumeMin_6_emmedia(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{

}

// 调用格式: SDT_BOOL (系统音量).取音量, 命令说明: "获取左声道和右声道的音量，成功返回真，失败返回假"
// 参数<1>: &左声道音量 SDT_INT, 参数说明: "该参数接收左声道的音量"
// 参数<2>: &右声道音量 SDT_INT, 参数说明: "该参数接收右声道的音量"
EMMEDIA_EXTERN_C void emmedia_GetVolume_7_emmedia(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{
    PINT     arg1 = pArgInf[1].m_pInt;
    PINT     arg2 = pArgInf[2].m_pInt;

}

// 调用格式: SDT_BOOL (系统音量).置音量, 命令说明: "设置左声道和右声道的音量。成功返回真，失败返回假"
// 参数<1>: 左声道音量 SDT_INT, 参数说明: "要设置的左声道的音量"
// 参数<2>: 右声道音量 SDT_INT, 参数说明: "要设置的右声道的音量"
EMMEDIA_EXTERN_C void emmedia_SetVolume_8_emmedia(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{
    INT      arg1 = pArgInf[1].m_int;
    INT      arg2 = pArgInf[2].m_int;

}

// 调用格式: SDT_INT (系统音量).取设备号, 命令说明: "获取音量控制或静音控制的标识号"
// 参数<1>: 设备标识 SDT_INT, 参数说明: "0表示音量，非0表示静音"
EMMEDIA_EXTERN_C void emmedia_GetDeviceID_9_emmedia(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{
    INT      arg1 = pArgInf[1].m_int;

}

// 调用格式: SDT_TEXT (系统音量).取设备名, 命令说明: "获取设备的名称"
// 参数<1>: 设备标识 SDT_INT, 参数说明: "0表示音量，非0表示静音"
EMMEDIA_EXTERN_C void emmedia_GetDeviceName_10_emmedia(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{
    INT      arg1 = pArgInf[1].m_int;

}

// 调用格式: SDT_BOOL (录音).录制, 命令说明: "开始录制。成功返回真，失败返回假"
// 无参数
EMMEDIA_EXTERN_C void emmedia_Record_11_emmedia(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{

}

// 调用格式: SDT_BOOL (录音).是否在录制, 命令说明: "判断是否正在录制。成功返回真，失败返回假"
// 无参数
EMMEDIA_EXTERN_C void emmedia_IsRecording_12_emmedia(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{

}

// 调用格式: SDT_BOOL (录音).暂停, 命令说明: "暂停录制。成功返回真，失败返回假"
// 无参数
EMMEDIA_EXTERN_C void emmedia_Pause_13_emmedia(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{

}

// 调用格式: SDT_BOOL (录音).继续, 命令说明: "恢复一个被暂停的录制。成功返回真，失败返回假"
// 无参数
EMMEDIA_EXTERN_C void emmedia_Continue_14_emmedia(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{

}

// 调用格式: SDT_BOOL (录音).停止, 命令说明: "停止录制。成功返回真，失败返回假"
// 无参数
EMMEDIA_EXTERN_C void emmedia_Stop_15_emmedia(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{

}

// 调用格式: SDT_BOOL (录音).置格式, 命令说明: "设置录音的格式，注意参数的正确性，成功返回真，失败返回假。注意：本方法必须在使用\"录音\"方法前使用"
// 参数<1>: 线路数 SDT_INT, 参数说明: "本参数为录音的线路数，为以下常量值之一：1、#单声道；2、立体声双声道"
// 参数<2>: 采样率 SDT_INT, 参数说明: "本参数为每秒采样率(hertz)，为以下常量值之一：8000、#8000HZ；11025、#11025HZ；22050、#22050HZ；44100、#44100HZ"
// 参数<3>: 采样大小 SDT_INT, 参数说明: "本参数为采样大小，以位为单位，为以下常量值之一：8、#8位；16、#16位"
EMMEDIA_EXTERN_C void emmedia_SetWaveFormat_16_emmedia(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{
    INT      arg1 = pArgInf[1].m_int;
    INT      arg2 = pArgInf[2].m_int;
    INT      arg3 = pArgInf[3].m_int;

}

// 调用格式: SDT_BOOL (录音).取格式, 命令说明: "获取录音格式"
// 参数<1>: &线路数 SDT_INT, 参数说明: "本参数为录音的线路数，为以下常量值之一：1、#单声道；2、立体声双声道"
// 参数<2>: &采样率 SDT_INT, 参数说明: "本参数为每秒采样率(hertz)，为以下常量值之一：11025、#11025HZ；22050、#22050HZ；44100、#44100HZ"
// 参数<3>: &采样大小 SDT_INT, 参数说明: "本参数为采样大小，以位为单位，为以下常量值之一：8、#8位；16、#16位"
EMMEDIA_EXTERN_C void emmedia_GetWaveFormat_17_emmedia(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{
    PINT     arg1 = pArgInf[1].m_pInt;
    PINT     arg2 = pArgInf[2].m_pInt;
    PINT     arg3 = pArgInf[3].m_pInt;

}

// 调用格式: SDT_BOOL (录音).保存文件, 命令说明: "以*.wav格式保存当前录制的声音到指定文件中，成功返回真，失败返回假"
// 参数<1>: 保存文件名 SDT_TEXT, 参数说明: NULL
// 参数<2>: 覆盖已有文件 SDT_BOOL, 参数说明: NULL
EMMEDIA_EXTERN_C void emmedia_SaveFile_18_emmedia(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{
    LPSTR    arg1 = pArgInf[1].m_pText;
    BOOL     arg2 = pArgInf[2].m_bool;

}

// 调用格式: SDT_INT (录音).取位置, 命令说明: "返回当前已录制的字节的(Bytes)位置, 失败返回-1"
// 无参数
EMMEDIA_EXTERN_C void emmedia_GetPosition_19_emmedia(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{

}

// 调用格式: SDT_INT (MIDI演奏).取设备数量, 命令说明: "获取当前可以使用MIDI设备的数量"
// 无参数
EMMEDIA_EXTERN_C void emmedia_GetDeviceNum_20_emmedia(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{

}

// 调用格式: SDT_TEXT (MIDI演奏).取设备名, 命令说明: "获取指定MIDI设备的名称"
// 参数<1>: 设备序号 SDT_INT, 参数说明: "设备序号从0至最大设备数减1，最大设备数可以用\"取设备数量\"命令获取"
EMMEDIA_EXTERN_C void emmedia_GetDeviceName_21_emmedia(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{
    INT      arg1 = pArgInf[1].m_int;

}

// 调用格式: SDT_BOOL (MIDI演奏).打开, 命令说明: "通过MIDI设备序号打开指定的MIDI设备，成功返回真，失败返回假"
// 参数<1>: 设备序号 SDT_INT, 参数说明: "设备序号从0至最大设备数减1，最大设备数可以用\"取设备数量\"命令获取"
EMMEDIA_EXTERN_C void emmedia_Open_22_emmedia(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{
    INT      arg1 = pArgInf[1].m_int;

}

// 调用格式: SDT_BOOL (MIDI演奏).是否打开, 命令说明: "判断MIDI设备是否打开，如打开返回真，否则返回假"
// 无参数
EMMEDIA_EXTERN_C void emmedia_IsOpen_23_emmedia(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{

}

// 调用格式: SDT_BOOL (MIDI演奏).关闭, 命令说明: "关闭当前MIDI设备，成功返回真，否则返回假"
// 无参数
EMMEDIA_EXTERN_C void emmedia_Close_24_emmedia(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{

}

// 调用格式: SDT_BOOL (MIDI演奏).置乐器, 命令说明: "以指定的乐器序号设置当前MIDI设置要演奏的乐器，成功返回真，失败返回假"
// 参数<1>: 乐器序号 SDT_INT, 参数说明: "乐器序号范围为1至最大支持乐器数，最大支持乐器数可以通过\"取乐器数量\"方法取得"
// 参数<2>: [通道 SDT_INT], 参数说明: "该参数指定设置乐器序号的通道，从0 ~ 15"
EMMEDIA_EXTERN_C void emmedia_SetInstrument_25_emmedia(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{
    INT      arg1 = pArgInf[1].m_int;
    INT      arg2 = pArgInf[2].m_int;

}

// 调用格式: SDT_INT (MIDI演奏).取乐器序号, 命令说明: "获取指定通道当前使用的乐器序号，乐器序号从1开始"
// 参数<1>: [通道 SDT_INT], 参数说明: "该参数指定乐器序号的通道，从0 ~ 15"
EMMEDIA_EXTERN_C void emmedia_GetInstrument_26_emmedia(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{
    INT      arg1 = pArgInf[1].m_int;

}

// 调用格式: SDT_INT (MIDI演奏).取乐器数量, 命令说明: "获取当前所有支持的乐器的数量"
// 无参数
EMMEDIA_EXTERN_C void emmedia_GetInstrumentNum_27_emmedia(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{

}

// 调用格式: SDT_TEXT (MIDI演奏).取乐器名, 命令说明: "通过指定乐器序号获取乐器名称"
// 参数<1>: 乐器序号 SDT_INT, 参数说明: "乐器序号范围为1至最大支持乐器数，最大支持乐器数可以通过\"取乐器数量\"方法取得"
EMMEDIA_EXTERN_C void emmedia_GetInstrumentName_28_emmedia(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{
    INT      arg1 = pArgInf[1].m_int;

}

// 调用格式: SDT_BOOL (MIDI演奏).奏, 命令说明: "演奏一个音符。成功返回真，失败返回假"
// 参数<1>: 音符 SDT_BYTE, 参数说明: "该参数范围为0至127。0表示音C，1表示C#，依次类推，127表示第10个8度的127"
// 参数<2>: [通道 SDT_INT], 参数说明: "该参数指定音符演奏的通道，从0 ~ 15"
EMMEDIA_EXTERN_C void emmedia_NoteOn_29_emmedia(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{
    BYTE     arg1 = pArgInf[1].m_byte;
    INT      arg2 = pArgInf[2].m_int;

}

// 调用格式: SDT_BOOL (MIDI演奏).停奏, 命令说明: "停止演奏一个音符。成功返回真，失败返回假"
// 参数<1>: 音符 SDT_BYTE, 参数说明: "该参数范围为0至127。0表示音C，1表示C#，依次类推，127表示第10个8度的127"
// 参数<2>: [通道 SDT_INT], 参数说明: "该参数指定音符停止演奏的通道，从0 ~ 15"
EMMEDIA_EXTERN_C void emmedia_NoteOff_30_emmedia(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{
    BYTE     arg1 = pArgInf[1].m_byte;
    INT      arg2 = pArgInf[2].m_int;

}

// 调用格式: SDT_BOOL (录音音波).打开, 命令说明: "打开设备接收声波输入，成功返回真，失败返回假"
// 无参数
EMMEDIA_EXTERN_C void emmedia_Open_31_emmedia(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{

}

// 调用格式: SDT_BOOL (录音音波).是否打开, 命令说明: "判断设备是否打开，如打开返回真，否则返回假"
// 无参数
EMMEDIA_EXTERN_C void emmedia_IsOpen_32_emmedia(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{

}

// 调用格式: SDT_BOOL (录音音波).关闭, 命令说明: "关闭已打开设备，成功返回真，失败返回假"
// 无参数
EMMEDIA_EXTERN_C void emmedia_Close_33_emmedia(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{

}

// 本命令被隐藏, 原始名字 = "置间隔"
// 调用格式: SDT_BOOL (录音音波).置间隔, 命令说明: NULL
// 参数<1>: 间隔 SDT_INT, 参数说明: NULL
EMMEDIA_EXTERN_C void emmedia_SetWaveInInterval_34_emmedia(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{
    INT      arg1 = pArgInf[1].m_int;

}

// 本命令被隐藏, 原始名字 = "取间隔"
// 调用格式: SDT_INT (录音音波).取间隔, 命令说明: NULL
// 无参数
EMMEDIA_EXTERN_C void emmedia_GetWaveInInterval_35_emmedia(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{

}

// 调用格式: SDT_INT (录音音波).取上限, 命令说明: "取可以接收的声波值的最大频段（也可以理解为数组上限）"
// 无参数
EMMEDIA_EXTERN_C void emmedia_GetUpBound_36_emmedia(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{

}

// 调用格式: SDT_BOOL (录音音波).取声波值, 命令说明: "获取指定频段的声波值。成功返回真，失败返回假"
// 参数<1>: &声波值 SDT_SHORT, 参数说明: "接收声波值的变量"
// 参数<2>: 复制序号 SDT_INT, 参数说明: "要声波值的序号，序号的范围为0至最大频段减1，最大频段可以通过\"取上限\"方法获取"
EMMEDIA_EXTERN_C void emmedia_GetWaveValue_37_emmedia(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{
    PSHORT   arg1 = pArgInf[1].m_pShort;
    INT      arg2 = pArgInf[2].m_int;

}

// 调用格式: SDT_BOOL (CD播放).打开, 命令说明: "打开指定的光驱。成功返回真，失败返回假"
// 参数<1>: 光驱盘符 SDT_TEXT, 参数说明: "该参数指定要控制的光驱的盘符，必须为如下格式: \"H:\\\"，以下格式是非法的： \"H\", \"H:\"；如果提供空文本，表示打开默认第一个光驱"
EMMEDIA_EXTERN_C void emmedia_Open_38_emmedia(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{
    LPSTR    arg1 = pArgInf[1].m_pText;

}

// 调用格式: SDT_BOOL (CD播放).关闭, 命令说明: "关闭光驱。成功返回真，失败返回假"
// 无参数
EMMEDIA_EXTERN_C void emmedia_Close_39_emmedia(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{

}

// 本命令被隐藏, 原始名字 = "开关仓门"
// 调用格式: SDT_BOOL (CD播放).开关仓门, 命令说明: "打开/关闭光驱仓门。成功返回真，失败返回假"
// 无参数
EMMEDIA_EXTERN_C void emmedia_DoorPower_40_emmedia(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{

}

// 调用格式: SDT_BOOL (CD播放).开仓门, 命令说明: "打开光驱仓门。成功返回真，失败返回假"
// 无参数
EMMEDIA_EXTERN_C void emmedia_DoorOpen_41_emmedia(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{

}

// 调用格式: SDT_BOOL (CD播放).关仓门, 命令说明: "关闭光驱仓门。成功返回真，失败返回假"
// 无参数
EMMEDIA_EXTERN_C void emmedia_DoorClose_42_emmedia(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{

}

// 调用格式: SDT_BOOL (CD播放).是否有CD, 命令说明: "判断当前光驱中是否有光盘，如果有返回真，否则返回假"
// 无参数
EMMEDIA_EXTERN_C void emmedia_HasCD_43_emmedia(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{

}

// 调用格式: SDT_INT (CD播放).取状态, 命令说明: "返回当前光驱的状态，为以下常量值之一：0、#未知状态；1、#播放状态；2、#停止状态；3、#弹出状态和无光盘状态"
// 无参数
EMMEDIA_EXTERN_C void emmedia_GetMode_44_emmedia(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{

}

// 调用格式: SDT_BOOL (CD播放).播放, 命令说明: "开始播放，成功返回真，失败返回假"
// 无参数
EMMEDIA_EXTERN_C void emmedia_Play_45_emmedia(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{

}

// 调用格式: SDT_BOOL (CD播放).暂停, 命令说明: "暂停播放，成功返回真，失败返回假"
// 无参数
EMMEDIA_EXTERN_C void emmedia_Pause_46_emmedia(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{

}

// 调用格式: SDT_BOOL (CD播放).恢复, 命令说明: "恢复播放，成功返回真，失败返回假"
// 无参数
EMMEDIA_EXTERN_C void emmedia_Resume_47_emmedia(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{

}

// 调用格式: SDT_BOOL (CD播放).停止, 命令说明: "停止播放，成功返回真，失败返回假"
// 无参数
EMMEDIA_EXTERN_C void emmedia_Stop_48_emmedia(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{

}

// 调用格式: SDT_BOOL (CD播放).跳到, 命令说明: "跳到指定曲目的指定位置。成功返回真，失败返回假"
// 参数<1>: 曲目号 SDT_INT, 参数说明: "该参数为要转到的曲目号"
EMMEDIA_EXTERN_C void emmedia_JumpTo_49_emmedia(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{
    INT      arg1 = pArgInf[1].m_int;

}

// 本命令被隐藏, 原始名字 = "移到"
// 调用格式: SDT_BOOL (CD播放).移到, 命令说明: "将播放位置移动到当前光盘的指定位置。成功返回真，失败返回假"
// 参数<1>: 跳转位置 SDT_INT, 参数说明: "该位置可以为CD上的任意一个位置，以毫秒为单位"
EMMEDIA_EXTERN_C void emmedia_SeekTo_50_emmedia(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{
    INT      arg1 = pArgInf[1].m_int;

}

// 调用格式: SDT_BOOL (CD播放).取位置, 命令说明: "获取当前的播放位置，成功返回真，失败返回假"
// 参数<1>: &曲目号 SDT_INT, 参数说明: "该参数接收曲目号"
// 参数<2>: &位置 SDT_INT, 参数说明: "该参数为当前播放在相应曲目中的位置，以毫秒为单位"
EMMEDIA_EXTERN_C void emmedia_GetPosition_51_emmedia(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{
    PINT     arg1 = pArgInf[1].m_pInt;
    PINT     arg2 = pArgInf[2].m_pInt;

}

// 调用格式: SDT_INT (CD播放).取曲目数, 命令说明: "获取当前CD光盘中的曲目数量"
// 无参数
EMMEDIA_EXTERN_C void emmedia_GetCount_52_emmedia(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{

}

// 调用格式: SDT_INT (CD播放).取长度, 命令说明: "获取指定曲目的时间长度，以毫秒为单位"
// 参数<1>: 曲目号 SDT_INT, 参数说明: "该参数为要取长度的曲目号"
EMMEDIA_EXTERN_C void emmedia_GetLength_53_emmedia(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{
    INT      arg1 = pArgInf[1].m_int;

}

// 调用格式: SDT_INT (CD播放).取总长度, 命令说明: "获取全部曲目的时间长度的总和，以毫秒为单位"
// 无参数
EMMEDIA_EXTERN_C void emmedia_GetFullLength_54_emmedia(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{

}

// 调用格式: SDT_TEXT (CD播放).取别名, 命令说明: "获取当前对象使用的MCI控制别名"
// 无参数
EMMEDIA_EXTERN_C void emmedia_GetMCIAlias_55_emmedia(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{

}

// 调用格式: SDT_BOOL (媒体播放).打开, 命令说明: "打开指定媒体文件。成功返回真，失败返回假"
// 参数<1>: 文件名 SDT_TEXT, 参数说明: "该参数为要打开的媒体文件名"
EMMEDIA_EXTERN_C void emmedia_Open_56_emmedia(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{
    LPSTR    arg1 = pArgInf[1].m_pText;

}

// 调用格式: SDT_BOOL (媒体播放).关闭, 命令说明: "关闭当前播放的媒体。成功返回真，失败返回假"
// 无参数
EMMEDIA_EXTERN_C void emmedia_Close_57_emmedia(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{

}

// 调用格式: SDT_BOOL (媒体播放).是否视频, 命令说明: "判断当前播放的内容是否是视频。如果是返回真，否则返回假"
// 无参数
EMMEDIA_EXTERN_C void emmedia_IsVideo_58_emmedia(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{

}

// 调用格式: SDT_INT (媒体播放).取句柄, 命令说明: "获取当前视频播放窗口的Windows 窗口句柄"
// 无参数
EMMEDIA_EXTERN_C void emmedia_GetHwnd_59_emmedia(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{

}

// 调用格式: SDT_BOOL (媒体播放).置句柄, 命令说明: "设置当前视频播放窗口的Windows 窗口句柄，成功返回真，失败返回假"
// 参数<1>: 窗口句柄 SDT_INT, 参数说明: "要设置的窗口句柄"
EMMEDIA_EXTERN_C void emmedia_SetHwnd_60_emmedia(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{
    INT      arg1 = pArgInf[1].m_int;

}

// 调用格式: SDT_INT (媒体播放).取状态, 命令说明: "获取当前的播放状态，为以下常量值之一：0、#未知状态；1、#播放状态；2、#停止状态；3、#暂停状态"
// 无参数
EMMEDIA_EXTERN_C void emmedia_GetMode_61_emmedia(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{

}

// 调用格式: SDT_INT (媒体播放).取长度, 命令说明: "获取当前媒体文件的长度"
// 无参数
EMMEDIA_EXTERN_C void emmedia_GetLength_62_emmedia(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{

}

// 调用格式: SDT_INT (媒体播放).取位置, 命令说明: "获取当前的播放位置"
// 无参数
EMMEDIA_EXTERN_C void emmedia_GetPosition_63_emmedia(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{

}

// 调用格式: SDT_INT (媒体播放).取总时间, 命令说明: "以秒为单位获取总播放时间"
// 无参数
EMMEDIA_EXTERN_C void emmedia_GetTotalSec_64_emmedia(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{

}

// 调用格式: SDT_INT (媒体播放).取比率, 命令说明: "获取每一帧的比率"
// 无参数
EMMEDIA_EXTERN_C void emmedia_GetFrameRate_65_emmedia(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{

}

// 调用格式: SDT_BOOL (媒体播放).播放, 命令说明: "从指定位置开始播放。成功返回真，失败返回假"
// 参数<1>: 播放位置 SDT_INT, 参数说明: "该参数为播放位置，以毫秒为单位。默认为-1，表示从当前处播放"
// 参数<2>: [播放窗口 SDT_INT], 参数说明: "指定播放窗口的窗口句柄。如果未指定此参数，或句柄非法，则自动使用之前“置句柄”指定的窗口句柄。"
EMMEDIA_EXTERN_C void emmedia_Play_66_emmedia(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{
    INT      arg1 = pArgInf[1].m_int;
    INT      arg2 = pArgInf[2].m_int;

}

// 调用格式: SDT_BOOL (媒体播放).暂停, 命令说明: "暂停播放。成功返回真，失败返回假"
// 无参数
EMMEDIA_EXTERN_C void emmedia_Pause_67_emmedia(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{

}

// 调用格式: SDT_BOOL (媒体播放).停止, 命令说明: "停止播放。成功返回真，失败返回假"
// 无参数
EMMEDIA_EXTERN_C void emmedia_Stop_68_emmedia(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{

}

// 调用格式: SDT_TEXT (媒体播放).取别名, 命令说明: "获取当前对象使用的MCI别名"
// 无参数
EMMEDIA_EXTERN_C void emmedia_GetMCIAlias_69_emmedia(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{

}

// 调用格式: SDT_INT (CD播放).取起始时间, 命令说明: "获取指定曲目的起始时间"
// 参数<1>: 曲目号 SDT_INT, 参数说明: "该参数为曲目的序号"
EMMEDIA_EXTERN_C void emmedia_GetTrackBeginTime_70_emmedia(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{
    INT      arg1 = pArgInf[1].m_int;

}

// 调用格式: SDT_BOOL (音频转换).初始化, 命令说明: "初始化音频转换。成功返回真，失败返回假。在使用本对象前，必须保证成功调用过本方法"
// 无参数
EMMEDIA_EXTERN_C void emmedia_Init_71_emmedia(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{

}

// 调用格式: SDT_INT (音频转换).取编码数, 命令说明: "获取当前编码器可用的编码格式数量。失败返回0"
// 无参数
EMMEDIA_EXTERN_C void emmedia_GetCodeCount_72_emmedia(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{

}

// 调用格式: SDT_BOOL (音频转换).取编码信息, 命令说明: "根据当前编码器的指定编码格式索引获取编码格式的相关信息。成功返回真，失败返回假"
// 参数<1>: 编码索引 SDT_INT, 参数说明: "编码格式的索引，从0 ~ 编码数量 - 1"
// 参数<2>: &是否为单声道 SDT_BOOL, 参数说明: "判断是否为单声道，如不是则是双声道"
// 参数<3>: &采样频率 SDT_INT, 参数说明: "每秒的采样频率，以kHz为单位"
// 参数<4>: &音频大小 SDT_INT, 参数说明: "每秒音频的大小，以kbps为单位"
EMMEDIA_EXTERN_C void emmedia_GetCodeInfo_73_emmedia(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{
    INT      arg1 = pArgInf[1].m_int;
    BOOL*    arg2 = pArgInf[2].m_pBool;
    PINT     arg3 = pArgInf[3].m_pInt;
    PINT     arg4 = pArgInf[4].m_pInt;

}

// 调用格式: SDT_BOOL (音频转换).转换, 命令说明: "转换音频文件。成功返回真，失败返回假"
// 参数<1>: 源文件名 SDT_TEXT, 参数说明: "要转换的源文件文件名"
// 参数<2>: 目标文件名 SDT_TEXT, 参数说明: "要转换的目的文件文件名，先前的文件将被复写"
// 参数<3>: 编码索引 SDT_INT, 参数说明: "编码格式的索引，从0 ~ 编码数量 - 1"
EMMEDIA_EXTERN_C void emmedia_Convert_74_emmedia(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{
    LPSTR    arg1 = pArgInf[1].m_pText;
    LPSTR    arg2 = pArgInf[2].m_pText;
    INT      arg3 = pArgInf[3].m_int;

}

// 调用格式: SDT_INT (音频转换).取编码器数, 命令说明: "获取当前可用的编码器的数量。失败返回0"
// 无参数
EMMEDIA_EXTERN_C void emmedia_GetEncoderCount_75_emmedia(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{

}

// 调用格式: SDT_TEXT (音频转换).取编码器名, 命令说明: "获取指定编码器索引对应的编码器名称。失败返回空文本"
// 参数<1>: 编码器索引 SDT_INT, 参数说明: "编码器索引从0 ~ 编码器数量 -1"
EMMEDIA_EXTERN_C void emmedia_GetEncoderName_76_emmedia(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{
    INT      arg1 = pArgInf[1].m_int;

}

// 调用格式: SDT_TEXT (音频转换).取当前编码器, 命令说明: "获取当前使用的编码器的名称。"
// 无参数
EMMEDIA_EXTERN_C void emmedia_GetCurrentEncoder_77_emmedia(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{

}

// 调用格式: SDT_BOOL (音频转换).置当前编码器, 命令说明: "设置当前使用的编码器的名称。成功返回真，失败返回假"
// 参数<1>: 编码器名称 SDT_TEXT, 参数说明: "编码器的名称。"
EMMEDIA_EXTERN_C void emmedia_SetCurrentEncoder_78_emmedia(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{
    LPSTR    arg1 = pArgInf[1].m_pText;

}

// 调用格式: SDT_BOOL (媒体播放).置音量, 命令说明: "设置当前播放媒体的音量，不影响系统音量。左右声道中只要有一个设置失败，就会返回假。"
// 参数<1>: [左声道音量 SDT_INT], 参数说明: "范围为0-100，0为静音，100为最大音量。如果本参数被省略或超出范围，则不修改此声道音量。"
// 参数<2>: [右声道音量 SDT_INT], 参数说明: "范围为0-100，0为静音，100为最大音量。如果本参数被省略或超出范围，则不修改此声道音量。"
EMMEDIA_EXTERN_C void emmedia_SetVolume_79_emmedia(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{
    INT      arg1 = pArgInf[1].m_int;
    INT      arg2 = pArgInf[2].m_int;

}

// 调用格式: SDT_BOOL (媒体播放).取音量, 命令说明: "获取当前播放音量（非系统音量）。左右声道中只要有一个获取失败，就会返回假。"
// 参数<1>: [&左声道音量 SDT_INT], 参数说明: "范围为0-100，0为静音，100为最大音量。如果本参数被省略，则不返回此声道音量。"
// 参数<2>: [&右声道音量 SDT_INT], 参数说明: "范围为0-100，0为静音，100为最大音量。如果本参数被省略，则不返回此声道音量。"
EMMEDIA_EXTERN_C void emmedia_GetVolume_80_emmedia(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{
    PINT     arg1 = pArgInf[1].m_pInt;
    PINT     arg2 = pArgInf[2].m_pInt;

}

