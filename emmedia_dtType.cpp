#include "include_emmedia_header.h"

// 系统音量组件交互方法
EXTERN_C PFN_INTERFACE WINAPI emmedia_GetInterface_SysVolume(INT nInterfaceNO);

// 系统音量 下的组件被创建回调, 从易语言IDE选中组件拖放到窗口上触发
HUNIT WINAPI emmedia_ControlCreate_SysVolume(
    LPBYTE pAllPropertyData,            //   指向本窗口单元的已有属性数据, 由本窗口单元的ITF_GET_PROPERTY_DATA接口产生, 如果没有数据则为NULL
    INT nAllPropertyDataSize,           //   提供pAllPropertyData所指向数据的尺寸, 如果没有则为0
    DWORD dwStyle,                      //   预先设置的窗口风格
    HWND hParentWnd,                    //   父窗口句柄
    UINT uID,                           //   在父窗口中的ID
    HMENU hMenu,                        //   未使用
    INT x, INT y, INT cx, INT cy,       //   指定位置及尺寸
    DWORD dwWinFormID, DWORD dwUnitID,  //   本窗口单元所在窗口及本身的ID, 用作通知到系统
    HWND hDesignWnd,                    //   如果blInDesignMode为真, 则hDesignWnd提供所设计窗口的窗口句柄
    BOOL blInDesignMode                 //   说明是否被易语言IDE调用以进行可视化设计, 运行时为假
);

// 系统音量 下的更新属性方法
BOOL WINAPI emmedia_PropUpDate_SysVolume(HUNIT hUnit, INT nPropertyIndex);

// 系统音量 下的弹出对话框方法
BOOL WINAPI emmedia_PropPopDlg_SysVolume(HUNIT hUnit, INT nPropertyIndex, BOOL* pblModified, LPVOID pResultExtraData);

// 系统音量 属性被改变方法
BOOL WINAPI emmedia_PropChanged_SysVolume(HUNIT hUnit, INT nPropertyIndex,  // 被修改的属性索引
    UNIT_PROPERTY_VALUE* pPropertyVaule, // 用作修改的相应属性数据
    LPTSTR* ppszTipText);    //目前尚未使用

// 系统音量 下的获取全部属性方法
HGLOBAL WINAPI emmedia_PropGetDataAll_SysVolume(HUNIT hUnit);

// 系统音量 下的获取单个属性方法
BOOL WINAPI emmedia_PropGetData_SysVolume(HUNIT hUnit, INT nPropertyIndex, PUNIT_PROPERTY_VALUE pPropertyVaule);

// 系统音量 询问单元是否需要指定的按键信息, 如果需要, 返回真, 否则返回假
BOOL WINAPI emmedia_PropKetInfo_SysVolume(HUNIT hUnit, WORD wKey);

// 系统音量 取设计时组件被单击放置到窗体上时的默认创建尺寸
INT WINAPI emmedia_PropNotifyReceiver_SysVolume(INT nMsg, DWORD dwParam1, DWORD dwParam2);

// 录音组件交互方法
EXTERN_C PFN_INTERFACE WINAPI emmedia_GetInterface_WaveRecord(INT nInterfaceNO);

// 录音 下的组件被创建回调, 从易语言IDE选中组件拖放到窗口上触发
HUNIT WINAPI emmedia_ControlCreate_WaveRecord(
    LPBYTE pAllPropertyData,            //   指向本窗口单元的已有属性数据, 由本窗口单元的ITF_GET_PROPERTY_DATA接口产生, 如果没有数据则为NULL
    INT nAllPropertyDataSize,           //   提供pAllPropertyData所指向数据的尺寸, 如果没有则为0
    DWORD dwStyle,                      //   预先设置的窗口风格
    HWND hParentWnd,                    //   父窗口句柄
    UINT uID,                           //   在父窗口中的ID
    HMENU hMenu,                        //   未使用
    INT x, INT y, INT cx, INT cy,       //   指定位置及尺寸
    DWORD dwWinFormID, DWORD dwUnitID,  //   本窗口单元所在窗口及本身的ID, 用作通知到系统
    HWND hDesignWnd,                    //   如果blInDesignMode为真, 则hDesignWnd提供所设计窗口的窗口句柄
    BOOL blInDesignMode                 //   说明是否被易语言IDE调用以进行可视化设计, 运行时为假
);

// 录音 下的更新属性方法
BOOL WINAPI emmedia_PropUpDate_WaveRecord(HUNIT hUnit, INT nPropertyIndex);

// 录音 下的弹出对话框方法
BOOL WINAPI emmedia_PropPopDlg_WaveRecord(HUNIT hUnit, INT nPropertyIndex, BOOL* pblModified, LPVOID pResultExtraData);

// 录音 属性被改变方法
BOOL WINAPI emmedia_PropChanged_WaveRecord(HUNIT hUnit, INT nPropertyIndex,  // 被修改的属性索引
    UNIT_PROPERTY_VALUE* pPropertyVaule, // 用作修改的相应属性数据
    LPTSTR* ppszTipText);    //目前尚未使用

// 录音 下的获取全部属性方法
HGLOBAL WINAPI emmedia_PropGetDataAll_WaveRecord(HUNIT hUnit);

// 录音 下的获取单个属性方法
BOOL WINAPI emmedia_PropGetData_WaveRecord(HUNIT hUnit, INT nPropertyIndex, PUNIT_PROPERTY_VALUE pPropertyVaule);

// 录音 询问单元是否需要指定的按键信息, 如果需要, 返回真, 否则返回假
BOOL WINAPI emmedia_PropKetInfo_WaveRecord(HUNIT hUnit, WORD wKey);

// 录音 取设计时组件被单击放置到窗体上时的默认创建尺寸
INT WINAPI emmedia_PropNotifyReceiver_WaveRecord(INT nMsg, DWORD dwParam1, DWORD dwParam2);

// MIDI演奏组件交互方法
EXTERN_C PFN_INTERFACE WINAPI emmedia_GetInterface_MIDIPlay(INT nInterfaceNO);

// MIDI演奏 下的组件被创建回调, 从易语言IDE选中组件拖放到窗口上触发
HUNIT WINAPI emmedia_ControlCreate_MIDIPlay(
    LPBYTE pAllPropertyData,            //   指向本窗口单元的已有属性数据, 由本窗口单元的ITF_GET_PROPERTY_DATA接口产生, 如果没有数据则为NULL
    INT nAllPropertyDataSize,           //   提供pAllPropertyData所指向数据的尺寸, 如果没有则为0
    DWORD dwStyle,                      //   预先设置的窗口风格
    HWND hParentWnd,                    //   父窗口句柄
    UINT uID,                           //   在父窗口中的ID
    HMENU hMenu,                        //   未使用
    INT x, INT y, INT cx, INT cy,       //   指定位置及尺寸
    DWORD dwWinFormID, DWORD dwUnitID,  //   本窗口单元所在窗口及本身的ID, 用作通知到系统
    HWND hDesignWnd,                    //   如果blInDesignMode为真, 则hDesignWnd提供所设计窗口的窗口句柄
    BOOL blInDesignMode                 //   说明是否被易语言IDE调用以进行可视化设计, 运行时为假
);

// MIDI演奏 下的更新属性方法
BOOL WINAPI emmedia_PropUpDate_MIDIPlay(HUNIT hUnit, INT nPropertyIndex);

// MIDI演奏 下的弹出对话框方法
BOOL WINAPI emmedia_PropPopDlg_MIDIPlay(HUNIT hUnit, INT nPropertyIndex, BOOL* pblModified, LPVOID pResultExtraData);

// MIDI演奏 属性被改变方法
BOOL WINAPI emmedia_PropChanged_MIDIPlay(HUNIT hUnit, INT nPropertyIndex,  // 被修改的属性索引
    UNIT_PROPERTY_VALUE* pPropertyVaule, // 用作修改的相应属性数据
    LPTSTR* ppszTipText);    //目前尚未使用

// MIDI演奏 下的获取全部属性方法
HGLOBAL WINAPI emmedia_PropGetDataAll_MIDIPlay(HUNIT hUnit);

// MIDI演奏 下的获取单个属性方法
BOOL WINAPI emmedia_PropGetData_MIDIPlay(HUNIT hUnit, INT nPropertyIndex, PUNIT_PROPERTY_VALUE pPropertyVaule);

// MIDI演奏 询问单元是否需要指定的按键信息, 如果需要, 返回真, 否则返回假
BOOL WINAPI emmedia_PropKetInfo_MIDIPlay(HUNIT hUnit, WORD wKey);

// MIDI演奏 取设计时组件被单击放置到窗体上时的默认创建尺寸
INT WINAPI emmedia_PropNotifyReceiver_MIDIPlay(INT nMsg, DWORD dwParam1, DWORD dwParam2);

// 录音音波组件交互方法
EXTERN_C PFN_INTERFACE WINAPI emmedia_GetInterface_WaveInForm(INT nInterfaceNO);

// 录音音波 下的组件被创建回调, 从易语言IDE选中组件拖放到窗口上触发
HUNIT WINAPI emmedia_ControlCreate_WaveInForm(
    LPBYTE pAllPropertyData,            //   指向本窗口单元的已有属性数据, 由本窗口单元的ITF_GET_PROPERTY_DATA接口产生, 如果没有数据则为NULL
    INT nAllPropertyDataSize,           //   提供pAllPropertyData所指向数据的尺寸, 如果没有则为0
    DWORD dwStyle,                      //   预先设置的窗口风格
    HWND hParentWnd,                    //   父窗口句柄
    UINT uID,                           //   在父窗口中的ID
    HMENU hMenu,                        //   未使用
    INT x, INT y, INT cx, INT cy,       //   指定位置及尺寸
    DWORD dwWinFormID, DWORD dwUnitID,  //   本窗口单元所在窗口及本身的ID, 用作通知到系统
    HWND hDesignWnd,                    //   如果blInDesignMode为真, 则hDesignWnd提供所设计窗口的窗口句柄
    BOOL blInDesignMode                 //   说明是否被易语言IDE调用以进行可视化设计, 运行时为假
);

// 录音音波 下的更新属性方法
BOOL WINAPI emmedia_PropUpDate_WaveInForm(HUNIT hUnit, INT nPropertyIndex);

// 录音音波 下的弹出对话框方法
BOOL WINAPI emmedia_PropPopDlg_WaveInForm(HUNIT hUnit, INT nPropertyIndex, BOOL* pblModified, LPVOID pResultExtraData);

// 录音音波 属性被改变方法
BOOL WINAPI emmedia_PropChanged_WaveInForm(HUNIT hUnit, INT nPropertyIndex,  // 被修改的属性索引
    UNIT_PROPERTY_VALUE* pPropertyVaule, // 用作修改的相应属性数据
    LPTSTR* ppszTipText);    //目前尚未使用

// 录音音波 下的获取全部属性方法
HGLOBAL WINAPI emmedia_PropGetDataAll_WaveInForm(HUNIT hUnit);

// 录音音波 下的获取单个属性方法
BOOL WINAPI emmedia_PropGetData_WaveInForm(HUNIT hUnit, INT nPropertyIndex, PUNIT_PROPERTY_VALUE pPropertyVaule);

// 录音音波 询问单元是否需要指定的按键信息, 如果需要, 返回真, 否则返回假
BOOL WINAPI emmedia_PropKetInfo_WaveInForm(HUNIT hUnit, WORD wKey);

// 录音音波 取设计时组件被单击放置到窗体上时的默认创建尺寸
INT WINAPI emmedia_PropNotifyReceiver_WaveInForm(INT nMsg, DWORD dwParam1, DWORD dwParam2);

// CD播放组件交互方法
EXTERN_C PFN_INTERFACE WINAPI emmedia_GetInterface_CDPlay(INT nInterfaceNO);

// CD播放 下的组件被创建回调, 从易语言IDE选中组件拖放到窗口上触发
HUNIT WINAPI emmedia_ControlCreate_CDPlay(
    LPBYTE pAllPropertyData,            //   指向本窗口单元的已有属性数据, 由本窗口单元的ITF_GET_PROPERTY_DATA接口产生, 如果没有数据则为NULL
    INT nAllPropertyDataSize,           //   提供pAllPropertyData所指向数据的尺寸, 如果没有则为0
    DWORD dwStyle,                      //   预先设置的窗口风格
    HWND hParentWnd,                    //   父窗口句柄
    UINT uID,                           //   在父窗口中的ID
    HMENU hMenu,                        //   未使用
    INT x, INT y, INT cx, INT cy,       //   指定位置及尺寸
    DWORD dwWinFormID, DWORD dwUnitID,  //   本窗口单元所在窗口及本身的ID, 用作通知到系统
    HWND hDesignWnd,                    //   如果blInDesignMode为真, 则hDesignWnd提供所设计窗口的窗口句柄
    BOOL blInDesignMode                 //   说明是否被易语言IDE调用以进行可视化设计, 运行时为假
);

// CD播放 下的更新属性方法
BOOL WINAPI emmedia_PropUpDate_CDPlay(HUNIT hUnit, INT nPropertyIndex);

// CD播放 下的弹出对话框方法
BOOL WINAPI emmedia_PropPopDlg_CDPlay(HUNIT hUnit, INT nPropertyIndex, BOOL* pblModified, LPVOID pResultExtraData);

// CD播放 属性被改变方法
BOOL WINAPI emmedia_PropChanged_CDPlay(HUNIT hUnit, INT nPropertyIndex,  // 被修改的属性索引
    UNIT_PROPERTY_VALUE* pPropertyVaule, // 用作修改的相应属性数据
    LPTSTR* ppszTipText);    //目前尚未使用

// CD播放 下的获取全部属性方法
HGLOBAL WINAPI emmedia_PropGetDataAll_CDPlay(HUNIT hUnit);

// CD播放 下的获取单个属性方法
BOOL WINAPI emmedia_PropGetData_CDPlay(HUNIT hUnit, INT nPropertyIndex, PUNIT_PROPERTY_VALUE pPropertyVaule);

// CD播放 询问单元是否需要指定的按键信息, 如果需要, 返回真, 否则返回假
BOOL WINAPI emmedia_PropKetInfo_CDPlay(HUNIT hUnit, WORD wKey);

// CD播放 取设计时组件被单击放置到窗体上时的默认创建尺寸
INT WINAPI emmedia_PropNotifyReceiver_CDPlay(INT nMsg, DWORD dwParam1, DWORD dwParam2);

// 媒体播放组件交互方法
EXTERN_C PFN_INTERFACE WINAPI emmedia_GetInterface_MediaPlay(INT nInterfaceNO);

// 媒体播放 下的组件被创建回调, 从易语言IDE选中组件拖放到窗口上触发
HUNIT WINAPI emmedia_ControlCreate_MediaPlay(
    LPBYTE pAllPropertyData,            //   指向本窗口单元的已有属性数据, 由本窗口单元的ITF_GET_PROPERTY_DATA接口产生, 如果没有数据则为NULL
    INT nAllPropertyDataSize,           //   提供pAllPropertyData所指向数据的尺寸, 如果没有则为0
    DWORD dwStyle,                      //   预先设置的窗口风格
    HWND hParentWnd,                    //   父窗口句柄
    UINT uID,                           //   在父窗口中的ID
    HMENU hMenu,                        //   未使用
    INT x, INT y, INT cx, INT cy,       //   指定位置及尺寸
    DWORD dwWinFormID, DWORD dwUnitID,  //   本窗口单元所在窗口及本身的ID, 用作通知到系统
    HWND hDesignWnd,                    //   如果blInDesignMode为真, 则hDesignWnd提供所设计窗口的窗口句柄
    BOOL blInDesignMode                 //   说明是否被易语言IDE调用以进行可视化设计, 运行时为假
);

// 媒体播放 下的更新属性方法
BOOL WINAPI emmedia_PropUpDate_MediaPlay(HUNIT hUnit, INT nPropertyIndex);

// 媒体播放 下的弹出对话框方法
BOOL WINAPI emmedia_PropPopDlg_MediaPlay(HUNIT hUnit, INT nPropertyIndex, BOOL* pblModified, LPVOID pResultExtraData);

// 媒体播放 属性被改变方法
BOOL WINAPI emmedia_PropChanged_MediaPlay(HUNIT hUnit, INT nPropertyIndex,  // 被修改的属性索引
    UNIT_PROPERTY_VALUE* pPropertyVaule, // 用作修改的相应属性数据
    LPTSTR* ppszTipText);    //目前尚未使用

// 媒体播放 下的获取全部属性方法
HGLOBAL WINAPI emmedia_PropGetDataAll_MediaPlay(HUNIT hUnit);

// 媒体播放 下的获取单个属性方法
BOOL WINAPI emmedia_PropGetData_MediaPlay(HUNIT hUnit, INT nPropertyIndex, PUNIT_PROPERTY_VALUE pPropertyVaule);

// 媒体播放 询问单元是否需要指定的按键信息, 如果需要, 返回真, 否则返回假
BOOL WINAPI emmedia_PropKetInfo_MediaPlay(HUNIT hUnit, WORD wKey);

// 媒体播放 取设计时组件被单击放置到窗体上时的默认创建尺寸
INT WINAPI emmedia_PropNotifyReceiver_MediaPlay(INT nMsg, DWORD dwParam1, DWORD dwParam2);

// 音频转换组件交互方法
EXTERN_C PFN_INTERFACE WINAPI emmedia_GetInterface_AudioConvert(INT nInterfaceNO);

// 音频转换 下的组件被创建回调, 从易语言IDE选中组件拖放到窗口上触发
HUNIT WINAPI emmedia_ControlCreate_AudioConvert(
    LPBYTE pAllPropertyData,            //   指向本窗口单元的已有属性数据, 由本窗口单元的ITF_GET_PROPERTY_DATA接口产生, 如果没有数据则为NULL
    INT nAllPropertyDataSize,           //   提供pAllPropertyData所指向数据的尺寸, 如果没有则为0
    DWORD dwStyle,                      //   预先设置的窗口风格
    HWND hParentWnd,                    //   父窗口句柄
    UINT uID,                           //   在父窗口中的ID
    HMENU hMenu,                        //   未使用
    INT x, INT y, INT cx, INT cy,       //   指定位置及尺寸
    DWORD dwWinFormID, DWORD dwUnitID,  //   本窗口单元所在窗口及本身的ID, 用作通知到系统
    HWND hDesignWnd,                    //   如果blInDesignMode为真, 则hDesignWnd提供所设计窗口的窗口句柄
    BOOL blInDesignMode                 //   说明是否被易语言IDE调用以进行可视化设计, 运行时为假
);

// 音频转换 下的更新属性方法
BOOL WINAPI emmedia_PropUpDate_AudioConvert(HUNIT hUnit, INT nPropertyIndex);

// 音频转换 下的弹出对话框方法
BOOL WINAPI emmedia_PropPopDlg_AudioConvert(HUNIT hUnit, INT nPropertyIndex, BOOL* pblModified, LPVOID pResultExtraData);

// 音频转换 属性被改变方法
BOOL WINAPI emmedia_PropChanged_AudioConvert(HUNIT hUnit, INT nPropertyIndex,  // 被修改的属性索引
    UNIT_PROPERTY_VALUE* pPropertyVaule, // 用作修改的相应属性数据
    LPTSTR* ppszTipText);    //目前尚未使用

// 音频转换 下的获取全部属性方法
HGLOBAL WINAPI emmedia_PropGetDataAll_AudioConvert(HUNIT hUnit);

// 音频转换 下的获取单个属性方法
BOOL WINAPI emmedia_PropGetData_AudioConvert(HUNIT hUnit, INT nPropertyIndex, PUNIT_PROPERTY_VALUE pPropertyVaule);

// 音频转换 询问单元是否需要指定的按键信息, 如果需要, 返回真, 否则返回假
BOOL WINAPI emmedia_PropKetInfo_AudioConvert(HUNIT hUnit, WORD wKey);

// 音频转换 取设计时组件被单击放置到窗体上时的默认创建尺寸
INT WINAPI emmedia_PropNotifyReceiver_AudioConvert(INT nMsg, DWORD dwParam1, DWORD dwParam2);

#ifndef __E_STATIC_LIB
// 系统音量  下的方法索引
static INT s_dtCmdIndexemmedia_SysVolume_static_var_00[] = 
{
    0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 
    10, 
};
static int s_dtCmdIndexemmedia_SysVolume_static_var_count_00 = sizeof(s_dtCmdIndexemmedia_SysVolume_static_var_00) / sizeof(s_dtCmdIndexemmedia_SysVolume_static_var_00[0]);

// 录音  下的方法索引
static INT s_dtCmdIndexemmedia_WaveRecord_static_var_01[] = 
{
    11, 12, 13, 14, 15, 16, 17, 18, 19, 
};
static int s_dtCmdIndexemmedia_WaveRecord_static_var_count_01 = sizeof(s_dtCmdIndexemmedia_WaveRecord_static_var_01) / sizeof(s_dtCmdIndexemmedia_WaveRecord_static_var_01[0]);

// MIDI演奏  下的方法索引
static INT s_dtCmdIndexemmedia_MIDIPlay_static_var_02[] = 
{
    20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 
    30, 
};
static int s_dtCmdIndexemmedia_MIDIPlay_static_var_count_02 = sizeof(s_dtCmdIndexemmedia_MIDIPlay_static_var_02) / sizeof(s_dtCmdIndexemmedia_MIDIPlay_static_var_02[0]);

// 录音音波  下的方法索引
static INT s_dtCmdIndexemmedia_WaveInForm_static_var_03[] = 
{
    31, 32, 33, 34, 35, 36, 37, 
};
static int s_dtCmdIndexemmedia_WaveInForm_static_var_count_03 = sizeof(s_dtCmdIndexemmedia_WaveInForm_static_var_03) / sizeof(s_dtCmdIndexemmedia_WaveInForm_static_var_03[0]);

// CD播放  下的方法索引
static INT s_dtCmdIndexemmedia_CDPlay_static_var_04[] = 
{
    38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 
    48, 49, 50, 51, 52, 53, 54, 70, 55, 
};
static int s_dtCmdIndexemmedia_CDPlay_static_var_count_04 = sizeof(s_dtCmdIndexemmedia_CDPlay_static_var_04) / sizeof(s_dtCmdIndexemmedia_CDPlay_static_var_04[0]);

// 媒体播放  下的方法索引
static INT s_dtCmdIndexemmedia_MediaPlay_static_var_05[] = 
{
    56, 58, 59, 60, 61, 62, 63, 64, 65, 66, 
    67, 68, 69, 57, 79, 80, 
};
static int s_dtCmdIndexemmedia_MediaPlay_static_var_count_05 = sizeof(s_dtCmdIndexemmedia_MediaPlay_static_var_05) / sizeof(s_dtCmdIndexemmedia_MediaPlay_static_var_05[0]);

// 音频转换  下的方法索引
static INT s_dtCmdIndexemmedia_AudioConvert_static_var_07[] = 
{
    71, 75, 76, 77, 78, 72, 73, 74, 
};
static int s_dtCmdIndexemmedia_AudioConvert_static_var_count_07 = sizeof(s_dtCmdIndexemmedia_AudioConvert_static_var_07) / sizeof(s_dtCmdIndexemmedia_AudioConvert_static_var_07[0]);

// 音量类型  下的枚举常量成员数组
static LIB_DATA_TYPE_ELEMENT s_objEventemmedia_VolumeType_static_var_06[] = 
{
    /*000*/ {SDT_INT, 0, "主音量", "Master", NULL, LES_HAS_DEFAULT_VALUE, (INT)0x00000004},
    /*001*/ {SDT_INT, 0, "波形", "WaveOut", NULL, LES_HAS_DEFAULT_VALUE, (INT)0x00001008},
    /*002*/ {SDT_INT, 0, "麦克", "Microphone", NULL, LES_HAS_DEFAULT_VALUE, (INT)0x00001003},
    /*003*/ {SDT_INT, 0, "CD", "CD", NULL, LES_HAS_DEFAULT_VALUE, (INT)0x00001005},
    /*004*/ {SDT_INT, 0, "电话线", "TelphoneLine", NULL, LES_HAS_DEFAULT_VALUE, (INT)0x00001006},
    /*005*/ {SDT_INT, 0, "线路输入", "LineIn", NULL, LES_HAS_DEFAULT_VALUE, (INT)0x00001002},
    /*006*/ {SDT_INT, 0, "软件合成器", "MIDI", NULL, LES_HAS_DEFAULT_VALUE, (INT)0x00001004},

};
static int s_objEventemmedia_VolumeType_static_var_count_06 = sizeof(s_objEventemmedia_VolumeType_static_var_06) / sizeof(s_objEventemmedia_VolumeType_static_var_06[0]);

// 事件参数, 所有事件参数都写到这里, 使用的时候 数组名 + 偏移就行了
static EVENT_ARG_INFO2 s_eventArgInfo_emmedia_SysVolume[] = 
{
    //1=参数名称,2=参数介绍,3=是否参考,4=参数类型SDT_    
    {},
};
// 系统音量  下的事件数组
static EVENT_INFO2 s_objEventemmedia_SysVolume_static_var_00[] = 
{
    //1=事件名称,2=事件详细解释,3=返回值类型,EV_RETURN_ 绝对不能定义成返回文本、字节集、复合类型等需要空间释放代码的数据类型
    //4=事件的参数数目,5=事件参数,6=返回值类型

    /*000*/ {"音量改变", "当前对象所控制的音量被改变后触发此事件", _EVENT_OS(OS_ALL) | EV_IS_VER2, 0, s_eventArgInfo_emmedia_SysVolume + 0, _SDT_NULL},
    /*001*/ {"静音改变", "当前对象所控制的静音被改变后触发此事件", _EVENT_OS(OS_ALL) | EV_IS_VER2, 0, s_eventArgInfo_emmedia_SysVolume + 0, _SDT_NULL},

};
static int s_objEventemmedia_SysVolume_static_var_count_00 = sizeof(s_objEventemmedia_SysVolume_static_var_00) / sizeof(s_objEventemmedia_SysVolume_static_var_00[0]);

LIB_DATA_TYPE_INFO g_DataType_emmedia_global_var[] = 
{
    //1=中文名字,2=英文名字,3=详细解释,4=命令数量,5=索引值,6=标志 LDT_
    //类型为窗口或菜单组件时有效 7=资源ID,8=事件数量,9=组件事件数组,10=属性数  11=属性数组 12=组件交互子程序
    //不为窗口、菜单组件或为枚举数据类型时才有效 13=成员数量,14=成员数据数组
    
/*000*/ { "系统音量", "SysVolume", "本对象提供对控制Windows系统音量的支持",
            s_dtCmdIndexemmedia_SysVolume_static_var_count_00, s_dtCmdIndexemmedia_SysVolume_static_var_00, _DT_OS(__OS_WIN) | LDT_WIN_UNIT | LDT_IS_FUNCTION_PROVIDER,
            0, s_objEventemmedia_SysVolume_static_var_count_00, s_objEventemmedia_SysVolume_static_var_00,
            NULL, NULL,
            emmedia_GetInterface_SysVolume,
            NULL, NULL
        },
/*001*/ { "录音", "WaveRecord", "本对象提供对Wave格式录音的支持",
            s_dtCmdIndexemmedia_WaveRecord_static_var_count_01, s_dtCmdIndexemmedia_WaveRecord_static_var_01, _DT_OS(__OS_WIN) | LDT_WIN_UNIT | LDT_IS_FUNCTION_PROVIDER,
            0, NULL, NULL,
            NULL, NULL,
            emmedia_GetInterface_WaveRecord,
            NULL, NULL
        },
/*002*/ { "MIDI演奏", "MIDIPlay", "本对象提供对MIDI演奏的基本支持",
            s_dtCmdIndexemmedia_MIDIPlay_static_var_count_02, s_dtCmdIndexemmedia_MIDIPlay_static_var_02, _DT_OS(__OS_WIN) | LDT_WIN_UNIT | LDT_IS_FUNCTION_PROVIDER,
            0, NULL, NULL,
            NULL, NULL,
            emmedia_GetInterface_MIDIPlay,
            NULL, NULL
        },
/*003*/ { "录音音波", "WaveInForm", "本对象提供对获取录音音波的支持",
            s_dtCmdIndexemmedia_WaveInForm_static_var_count_03, s_dtCmdIndexemmedia_WaveInForm_static_var_03, _DT_OS(__OS_WIN) | LDT_WIN_UNIT | LDT_IS_FUNCTION_PROVIDER,
            0, NULL, NULL,
            NULL, NULL,
            emmedia_GetInterface_WaveInForm,
            NULL, NULL
        },
/*004*/ { "CD播放", "CDPlay", "本对象提供对CD播放的支持。注意：本对象是架构在Windows MCI基础之上的，本对象提供了\"取别名\"方法，您可以通过该方法所得的文本，通过Windows API mciSendString等命令和本对象一起控制MCI播放",
            s_dtCmdIndexemmedia_CDPlay_static_var_count_04, s_dtCmdIndexemmedia_CDPlay_static_var_04, _DT_OS(__OS_WIN) | LDT_WIN_UNIT | LDT_IS_FUNCTION_PROVIDER,
            0, NULL, NULL,
            NULL, NULL,
            emmedia_GetInterface_CDPlay,
            NULL, NULL
        },
/*005*/ { "媒体播放", "MediaPlay", "本对象提供对媒体播放的支持，注意：本对象是架构在Windows MCI基础之上的，本对象提供了\"取别名\"方法，您可以通过该方法所得的文本，通过Windows API mciSendString等命令和本对象一起控制MCI播放。如果有媒体文件不能播放，请安装相应解码器",
            s_dtCmdIndexemmedia_MediaPlay_static_var_count_05, s_dtCmdIndexemmedia_MediaPlay_static_var_05, _DT_OS(__OS_WIN) | LDT_WIN_UNIT | LDT_IS_FUNCTION_PROVIDER,
            0, NULL, NULL,
            NULL, NULL,
            emmedia_GetInterface_MediaPlay,
            NULL, NULL
        },
/*006*/ { "音量类型", "VolumeType", NULL,
            NULL, NULL, _DT_OS(__OS_WIN) | LDT_ENUM,
            0, NULL, NULL,
            NULL, NULL,
            NULL,
            s_objEventemmedia_VolumeType_static_var_count_06, s_objEventemmedia_VolumeType_static_var_06
        },
/*007*/ { "音频转换", "AudioConvert", "本对象提供了对音频格式转换的支持。目前支持wav到mp3，mp3到wav以及wav到其他支持格式的转换。部分功能需要DirectX支持，转换的成功与否依赖于您Windows系统中关于音频的编码器和解码器。",
            s_dtCmdIndexemmedia_AudioConvert_static_var_count_07, s_dtCmdIndexemmedia_AudioConvert_static_var_07, _DT_OS(__OS_WIN) | LDT_WIN_UNIT | LDT_IS_FUNCTION_PROVIDER,
            0, NULL, NULL,
            NULL, NULL,
            emmedia_GetInterface_AudioConvert,
            NULL, NULL
        },
};
int g_DataType_emmedia_global_var_count = sizeof(g_DataType_emmedia_global_var) / sizeof(g_DataType_emmedia_global_var[0]);

#endif

// 系统音量组件交互方法
EXTERN_C PFN_INTERFACE WINAPI emmedia_GetInterface_SysVolume(INT nInterfaceNO)
{
    switch (nInterfaceNO)
    {
    case ITF_CREATE_UNIT:
    {
        // 创建组件
        return (PFN_INTERFACE)emmedia_ControlCreate_SysVolume;
    }
    case ITF_PROPERTY_UPDATE_UI:
    {
        // 说明属性目前可否被修改
        return (PFN_INTERFACE)emmedia_PropUpDate_SysVolume;
    }
    case ITF_DLG_INIT_CUSTOMIZE_DATA:
    {
        // 使用对话框设置自定义数据
        return (PFN_INTERFACE)emmedia_PropPopDlg_SysVolume;
    }
    case ITF_NOTIFY_PROPERTY_CHANGED:
    {
        // 通知某属性数据被用户修改
        return (PFN_INTERFACE)emmedia_PropChanged_SysVolume;
    }
    case ITF_GET_ALL_PROPERTY_DATA:
    {
        // 取全部属性数据
        return (PFN_INTERFACE)emmedia_PropGetDataAll_SysVolume;
    }
    case ITF_GET_PROPERTY_DATA:
    {
        // 取某属性数据
        return (PFN_INTERFACE)emmedia_PropGetData_SysVolume;
    }
    case ITF_GET_ICON_PROPERTY_DATA:
    {
        // 取窗口的图标属性数据(仅用于窗口)
        break;
    }
    case ITF_IS_NEED_THIS_KEY:
    {
        // 询问单元是否需要指定的按键信息, 用作窗口单元
        return (PFN_INTERFACE)emmedia_PropKetInfo_SysVolume;
    }
    case ITF_LANG_CNV:
    {
        // 组件数据语言转换
        break;
    }
    case ITF_MSG_FILTER:
    {
        // 消息过滤
        break;
    }
    case ITF_GET_NOTIFY_RECEIVER:
    {
        // 取组件的附加通知接收者(PFN_ON_NOTIFY_UNIT)
        return (PFN_INTERFACE)emmedia_PropNotifyReceiver_SysVolume;
    }
    default:
        return NULL;
    }
    return NULL;
}

// 系统音量 下的组件被创建回调, 从易语言IDE选中组件拖放到窗口上触发
HUNIT WINAPI emmedia_ControlCreate_SysVolume(
    LPBYTE pAllPropertyData,            //   指向本窗口单元的已有属性数据, 由本窗口单元的ITF_GET_PROPERTY_DATA接口产生, 如果没有数据则为NULL
    INT nAllPropertyDataSize,           //   提供pAllPropertyData所指向数据的尺寸, 如果没有则为0
    DWORD dwStyle,                      //   预先设置的窗口风格
    HWND hParentWnd,                    //   父窗口句柄
    UINT uID,                           //   在父窗口中的ID
    HMENU hMenu,                        //   未使用
    INT x, INT y, INT cx, INT cy,       //   指定位置及尺寸
    DWORD dwWinFormID, DWORD dwUnitID,  //   本窗口单元所在窗口及本身的ID, 用作通知到系统
    HWND hDesignWnd,                    //   如果blInDesignMode为真, 则hDesignWnd提供所设计窗口的窗口句柄
    BOOL blInDesignMode                 //   说明是否被易语言IDE调用以进行可视化设计, 运行时为假
)
{
    //TODO 在这里创建组件并返回
    HUNIT hUnit = 0;
    return hUnit;
}

// 系统音量 下的更新属性方法
BOOL WINAPI emmedia_PropUpDate_SysVolume(HUNIT hUnit, INT nPropertyIndex)
{
    // 如果指定属性目前可以被操作, 返回真, 否则返回假。
    return TRUE;
}

// 系统音量 下的弹出对话框方法
BOOL WINAPI emmedia_PropPopDlg_SysVolume(HUNIT hUnit, INT nPropertyIndex, BOOL* pblModified, LPVOID pResultExtraData)
{
    // 用作设置类型为UD_CUSTOMIZE的单元属性
    // 如果需要重新创建该单元才能修改单元外形, 请返回真
    // pblModified 不为NULL, 请在其中返回是否被用户真正修改(便于易语言IDE建立UNDO记录)
    *pblModified = false;
    return FALSE;
}

// 系统音量 属性被改变方法
BOOL WINAPI emmedia_PropChanged_SysVolume(HUNIT hUnit, INT nPropertyIndex,  // 被修改的属性索引
    UNIT_PROPERTY_VALUE* pPropertyVaule, // 用作修改的相应属性数据
    LPTSTR* ppszTipText)    //目前尚未使用
{
    // 通知某属性(非UD_CUSTOMIZE类别属性)数据被用户修改, 需要根据该修改相应更改
    // 内部数据及外形, 如果确实需要重新创建才能修改单元外形, 请返回真
    // 注意：必须进行所传入值的合法性校验
    
    switch (nPropertyIndex)
    {
    case 0: // 这个需要自己修改, 根据索引确定是哪个属性被修改
    {
        break;
    }
    default:
        return false;
    }
    return false;
}

// 系统音量 下的获取全部属性方法
HGLOBAL WINAPI emmedia_PropGetDataAll_SysVolume(HUNIT hUnit)
{
    // 自己处理一下属性, 返回HGLOBAL 内存句柄, 所有属性信息都写里面
    return 0;
}

// 系统音量 下的获取单个属性方法
BOOL WINAPI emmedia_PropGetData_SysVolume(HUNIT hUnit, INT nPropertyIndex, PUNIT_PROPERTY_VALUE pPropertyVaule)
{
    // 取某属性数据到pPropertyValue中, 成功返回真, 否则返回假
    // 注意：如果在设计时(由调用PFN_CREATE_UNIT时的blInDesignMode参数决定)
    // pPropertyValue必须返回所存储的值。如果在运行时(blInDesignMode为假)
    // 必须返回实际的当前实时值
    // 比如说, 编辑框窗口单元的"内容"属性, 设计时必须返回内部所保存的值
    // 而运行时就必须调用 GetWindowText 去实时获取
    
    switch (nPropertyIndex)
    {
    case 0: // 这个需要自己修改, 根据索引确定是哪个属性被修改
    {
        break;
    }
    default:
        return false;
    }
    return true;
}

// 系统音量 询问单元是否需要指定的按键信息, 如果需要, 返回真, 否则返回假
BOOL WINAPI emmedia_PropKetInfo_SysVolume(HUNIT hUnit, WORD wKey)
{
    // 询问单元是否需要指定的按键信息, 如果需要, 返回真, 否则返回假
    return FALSE;
}

// 系统音量 取设计时组件被单击放置到窗体上时的默认创建尺寸
INT WINAPI emmedia_PropNotifyReceiver_SysVolume(INT nMsg, DWORD dwParam1, DWORD dwParam2)
{
    switch (nMsg)
    {
    case NU_GET_CREATE_SIZE_IN_DESIGNER:
    {
        // 取设计时组件被单击放置到窗体上时的默认创建尺寸
        // dwParam1: 类型: INT*, 返回宽度(单位像素)
        // dwParam2: 类型: INT*, 返回高度(单位像素)
        // 成功返回1,失败返回0.
        // *((int*)dwParam1) = 宽度; // 组件拖放到ide上就使用这个默认宽度
        // *((int*)dwParam2) = 高度; // 组件拖放到ide上就使用这个默认高度
        // return 1; // 使用修改后的宽高, 自己根据需要修改
        break;
    }
    default:
        return 0;
    }
    return 0;
}

// 录音组件交互方法
EXTERN_C PFN_INTERFACE WINAPI emmedia_GetInterface_WaveRecord(INT nInterfaceNO)
{
    switch (nInterfaceNO)
    {
    case ITF_CREATE_UNIT:
    {
        // 创建组件
        return (PFN_INTERFACE)emmedia_ControlCreate_WaveRecord;
    }
    case ITF_PROPERTY_UPDATE_UI:
    {
        // 说明属性目前可否被修改
        return (PFN_INTERFACE)emmedia_PropUpDate_WaveRecord;
    }
    case ITF_DLG_INIT_CUSTOMIZE_DATA:
    {
        // 使用对话框设置自定义数据
        return (PFN_INTERFACE)emmedia_PropPopDlg_WaveRecord;
    }
    case ITF_NOTIFY_PROPERTY_CHANGED:
    {
        // 通知某属性数据被用户修改
        return (PFN_INTERFACE)emmedia_PropChanged_WaveRecord;
    }
    case ITF_GET_ALL_PROPERTY_DATA:
    {
        // 取全部属性数据
        return (PFN_INTERFACE)emmedia_PropGetDataAll_WaveRecord;
    }
    case ITF_GET_PROPERTY_DATA:
    {
        // 取某属性数据
        return (PFN_INTERFACE)emmedia_PropGetData_WaveRecord;
    }
    case ITF_GET_ICON_PROPERTY_DATA:
    {
        // 取窗口的图标属性数据(仅用于窗口)
        break;
    }
    case ITF_IS_NEED_THIS_KEY:
    {
        // 询问单元是否需要指定的按键信息, 用作窗口单元
        return (PFN_INTERFACE)emmedia_PropKetInfo_WaveRecord;
    }
    case ITF_LANG_CNV:
    {
        // 组件数据语言转换
        break;
    }
    case ITF_MSG_FILTER:
    {
        // 消息过滤
        break;
    }
    case ITF_GET_NOTIFY_RECEIVER:
    {
        // 取组件的附加通知接收者(PFN_ON_NOTIFY_UNIT)
        return (PFN_INTERFACE)emmedia_PropNotifyReceiver_WaveRecord;
    }
    default:
        return NULL;
    }
    return NULL;
}

// 录音 下的组件被创建回调, 从易语言IDE选中组件拖放到窗口上触发
HUNIT WINAPI emmedia_ControlCreate_WaveRecord(
    LPBYTE pAllPropertyData,            //   指向本窗口单元的已有属性数据, 由本窗口单元的ITF_GET_PROPERTY_DATA接口产生, 如果没有数据则为NULL
    INT nAllPropertyDataSize,           //   提供pAllPropertyData所指向数据的尺寸, 如果没有则为0
    DWORD dwStyle,                      //   预先设置的窗口风格
    HWND hParentWnd,                    //   父窗口句柄
    UINT uID,                           //   在父窗口中的ID
    HMENU hMenu,                        //   未使用
    INT x, INT y, INT cx, INT cy,       //   指定位置及尺寸
    DWORD dwWinFormID, DWORD dwUnitID,  //   本窗口单元所在窗口及本身的ID, 用作通知到系统
    HWND hDesignWnd,                    //   如果blInDesignMode为真, 则hDesignWnd提供所设计窗口的窗口句柄
    BOOL blInDesignMode                 //   说明是否被易语言IDE调用以进行可视化设计, 运行时为假
)
{
    //TODO 在这里创建组件并返回
    HUNIT hUnit = 0;
    return hUnit;
}

// 录音 下的更新属性方法
BOOL WINAPI emmedia_PropUpDate_WaveRecord(HUNIT hUnit, INT nPropertyIndex)
{
    // 如果指定属性目前可以被操作, 返回真, 否则返回假。
    return TRUE;
}

// 录音 下的弹出对话框方法
BOOL WINAPI emmedia_PropPopDlg_WaveRecord(HUNIT hUnit, INT nPropertyIndex, BOOL* pblModified, LPVOID pResultExtraData)
{
    // 用作设置类型为UD_CUSTOMIZE的单元属性
    // 如果需要重新创建该单元才能修改单元外形, 请返回真
    // pblModified 不为NULL, 请在其中返回是否被用户真正修改(便于易语言IDE建立UNDO记录)
    *pblModified = false;
    return FALSE;
}

// 录音 属性被改变方法
BOOL WINAPI emmedia_PropChanged_WaveRecord(HUNIT hUnit, INT nPropertyIndex,  // 被修改的属性索引
    UNIT_PROPERTY_VALUE* pPropertyVaule, // 用作修改的相应属性数据
    LPTSTR* ppszTipText)    //目前尚未使用
{
    // 通知某属性(非UD_CUSTOMIZE类别属性)数据被用户修改, 需要根据该修改相应更改
    // 内部数据及外形, 如果确实需要重新创建才能修改单元外形, 请返回真
    // 注意：必须进行所传入值的合法性校验
    
    switch (nPropertyIndex)
    {
    case 0: // 这个需要自己修改, 根据索引确定是哪个属性被修改
    {
        break;
    }
    default:
        return false;
    }
    return false;
}

// 录音 下的获取全部属性方法
HGLOBAL WINAPI emmedia_PropGetDataAll_WaveRecord(HUNIT hUnit)
{
    // 自己处理一下属性, 返回HGLOBAL 内存句柄, 所有属性信息都写里面
    return 0;
}

// 录音 下的获取单个属性方法
BOOL WINAPI emmedia_PropGetData_WaveRecord(HUNIT hUnit, INT nPropertyIndex, PUNIT_PROPERTY_VALUE pPropertyVaule)
{
    // 取某属性数据到pPropertyValue中, 成功返回真, 否则返回假
    // 注意：如果在设计时(由调用PFN_CREATE_UNIT时的blInDesignMode参数决定)
    // pPropertyValue必须返回所存储的值。如果在运行时(blInDesignMode为假)
    // 必须返回实际的当前实时值
    // 比如说, 编辑框窗口单元的"内容"属性, 设计时必须返回内部所保存的值
    // 而运行时就必须调用 GetWindowText 去实时获取
    
    switch (nPropertyIndex)
    {
    case 0: // 这个需要自己修改, 根据索引确定是哪个属性被修改
    {
        break;
    }
    default:
        return false;
    }
    return true;
}

// 录音 询问单元是否需要指定的按键信息, 如果需要, 返回真, 否则返回假
BOOL WINAPI emmedia_PropKetInfo_WaveRecord(HUNIT hUnit, WORD wKey)
{
    // 询问单元是否需要指定的按键信息, 如果需要, 返回真, 否则返回假
    return FALSE;
}

// 录音 取设计时组件被单击放置到窗体上时的默认创建尺寸
INT WINAPI emmedia_PropNotifyReceiver_WaveRecord(INT nMsg, DWORD dwParam1, DWORD dwParam2)
{
    switch (nMsg)
    {
    case NU_GET_CREATE_SIZE_IN_DESIGNER:
    {
        // 取设计时组件被单击放置到窗体上时的默认创建尺寸
        // dwParam1: 类型: INT*, 返回宽度(单位像素)
        // dwParam2: 类型: INT*, 返回高度(单位像素)
        // 成功返回1,失败返回0.
        // *((int*)dwParam1) = 宽度; // 组件拖放到ide上就使用这个默认宽度
        // *((int*)dwParam2) = 高度; // 组件拖放到ide上就使用这个默认高度
        // return 1; // 使用修改后的宽高, 自己根据需要修改
        break;
    }
    default:
        return 0;
    }
    return 0;
}

// MIDI演奏组件交互方法
EXTERN_C PFN_INTERFACE WINAPI emmedia_GetInterface_MIDIPlay(INT nInterfaceNO)
{
    switch (nInterfaceNO)
    {
    case ITF_CREATE_UNIT:
    {
        // 创建组件
        return (PFN_INTERFACE)emmedia_ControlCreate_MIDIPlay;
    }
    case ITF_PROPERTY_UPDATE_UI:
    {
        // 说明属性目前可否被修改
        return (PFN_INTERFACE)emmedia_PropUpDate_MIDIPlay;
    }
    case ITF_DLG_INIT_CUSTOMIZE_DATA:
    {
        // 使用对话框设置自定义数据
        return (PFN_INTERFACE)emmedia_PropPopDlg_MIDIPlay;
    }
    case ITF_NOTIFY_PROPERTY_CHANGED:
    {
        // 通知某属性数据被用户修改
        return (PFN_INTERFACE)emmedia_PropChanged_MIDIPlay;
    }
    case ITF_GET_ALL_PROPERTY_DATA:
    {
        // 取全部属性数据
        return (PFN_INTERFACE)emmedia_PropGetDataAll_MIDIPlay;
    }
    case ITF_GET_PROPERTY_DATA:
    {
        // 取某属性数据
        return (PFN_INTERFACE)emmedia_PropGetData_MIDIPlay;
    }
    case ITF_GET_ICON_PROPERTY_DATA:
    {
        // 取窗口的图标属性数据(仅用于窗口)
        break;
    }
    case ITF_IS_NEED_THIS_KEY:
    {
        // 询问单元是否需要指定的按键信息, 用作窗口单元
        return (PFN_INTERFACE)emmedia_PropKetInfo_MIDIPlay;
    }
    case ITF_LANG_CNV:
    {
        // 组件数据语言转换
        break;
    }
    case ITF_MSG_FILTER:
    {
        // 消息过滤
        break;
    }
    case ITF_GET_NOTIFY_RECEIVER:
    {
        // 取组件的附加通知接收者(PFN_ON_NOTIFY_UNIT)
        return (PFN_INTERFACE)emmedia_PropNotifyReceiver_MIDIPlay;
    }
    default:
        return NULL;
    }
    return NULL;
}

// MIDI演奏 下的组件被创建回调, 从易语言IDE选中组件拖放到窗口上触发
HUNIT WINAPI emmedia_ControlCreate_MIDIPlay(
    LPBYTE pAllPropertyData,            //   指向本窗口单元的已有属性数据, 由本窗口单元的ITF_GET_PROPERTY_DATA接口产生, 如果没有数据则为NULL
    INT nAllPropertyDataSize,           //   提供pAllPropertyData所指向数据的尺寸, 如果没有则为0
    DWORD dwStyle,                      //   预先设置的窗口风格
    HWND hParentWnd,                    //   父窗口句柄
    UINT uID,                           //   在父窗口中的ID
    HMENU hMenu,                        //   未使用
    INT x, INT y, INT cx, INT cy,       //   指定位置及尺寸
    DWORD dwWinFormID, DWORD dwUnitID,  //   本窗口单元所在窗口及本身的ID, 用作通知到系统
    HWND hDesignWnd,                    //   如果blInDesignMode为真, 则hDesignWnd提供所设计窗口的窗口句柄
    BOOL blInDesignMode                 //   说明是否被易语言IDE调用以进行可视化设计, 运行时为假
)
{
    //TODO 在这里创建组件并返回
    HUNIT hUnit = 0;
    return hUnit;
}

// MIDI演奏 下的更新属性方法
BOOL WINAPI emmedia_PropUpDate_MIDIPlay(HUNIT hUnit, INT nPropertyIndex)
{
    // 如果指定属性目前可以被操作, 返回真, 否则返回假。
    return TRUE;
}

// MIDI演奏 下的弹出对话框方法
BOOL WINAPI emmedia_PropPopDlg_MIDIPlay(HUNIT hUnit, INT nPropertyIndex, BOOL* pblModified, LPVOID pResultExtraData)
{
    // 用作设置类型为UD_CUSTOMIZE的单元属性
    // 如果需要重新创建该单元才能修改单元外形, 请返回真
    // pblModified 不为NULL, 请在其中返回是否被用户真正修改(便于易语言IDE建立UNDO记录)
    *pblModified = false;
    return FALSE;
}

// MIDI演奏 属性被改变方法
BOOL WINAPI emmedia_PropChanged_MIDIPlay(HUNIT hUnit, INT nPropertyIndex,  // 被修改的属性索引
    UNIT_PROPERTY_VALUE* pPropertyVaule, // 用作修改的相应属性数据
    LPTSTR* ppszTipText)    //目前尚未使用
{
    // 通知某属性(非UD_CUSTOMIZE类别属性)数据被用户修改, 需要根据该修改相应更改
    // 内部数据及外形, 如果确实需要重新创建才能修改单元外形, 请返回真
    // 注意：必须进行所传入值的合法性校验
    
    switch (nPropertyIndex)
    {
    case 0: // 这个需要自己修改, 根据索引确定是哪个属性被修改
    {
        break;
    }
    default:
        return false;
    }
    return false;
}

// MIDI演奏 下的获取全部属性方法
HGLOBAL WINAPI emmedia_PropGetDataAll_MIDIPlay(HUNIT hUnit)
{
    // 自己处理一下属性, 返回HGLOBAL 内存句柄, 所有属性信息都写里面
    return 0;
}

// MIDI演奏 下的获取单个属性方法
BOOL WINAPI emmedia_PropGetData_MIDIPlay(HUNIT hUnit, INT nPropertyIndex, PUNIT_PROPERTY_VALUE pPropertyVaule)
{
    // 取某属性数据到pPropertyValue中, 成功返回真, 否则返回假
    // 注意：如果在设计时(由调用PFN_CREATE_UNIT时的blInDesignMode参数决定)
    // pPropertyValue必须返回所存储的值。如果在运行时(blInDesignMode为假)
    // 必须返回实际的当前实时值
    // 比如说, 编辑框窗口单元的"内容"属性, 设计时必须返回内部所保存的值
    // 而运行时就必须调用 GetWindowText 去实时获取
    
    switch (nPropertyIndex)
    {
    case 0: // 这个需要自己修改, 根据索引确定是哪个属性被修改
    {
        break;
    }
    default:
        return false;
    }
    return true;
}

// MIDI演奏 询问单元是否需要指定的按键信息, 如果需要, 返回真, 否则返回假
BOOL WINAPI emmedia_PropKetInfo_MIDIPlay(HUNIT hUnit, WORD wKey)
{
    // 询问单元是否需要指定的按键信息, 如果需要, 返回真, 否则返回假
    return FALSE;
}

// MIDI演奏 取设计时组件被单击放置到窗体上时的默认创建尺寸
INT WINAPI emmedia_PropNotifyReceiver_MIDIPlay(INT nMsg, DWORD dwParam1, DWORD dwParam2)
{
    switch (nMsg)
    {
    case NU_GET_CREATE_SIZE_IN_DESIGNER:
    {
        // 取设计时组件被单击放置到窗体上时的默认创建尺寸
        // dwParam1: 类型: INT*, 返回宽度(单位像素)
        // dwParam2: 类型: INT*, 返回高度(单位像素)
        // 成功返回1,失败返回0.
        // *((int*)dwParam1) = 宽度; // 组件拖放到ide上就使用这个默认宽度
        // *((int*)dwParam2) = 高度; // 组件拖放到ide上就使用这个默认高度
        // return 1; // 使用修改后的宽高, 自己根据需要修改
        break;
    }
    default:
        return 0;
    }
    return 0;
}

// 录音音波组件交互方法
EXTERN_C PFN_INTERFACE WINAPI emmedia_GetInterface_WaveInForm(INT nInterfaceNO)
{
    switch (nInterfaceNO)
    {
    case ITF_CREATE_UNIT:
    {
        // 创建组件
        return (PFN_INTERFACE)emmedia_ControlCreate_WaveInForm;
    }
    case ITF_PROPERTY_UPDATE_UI:
    {
        // 说明属性目前可否被修改
        return (PFN_INTERFACE)emmedia_PropUpDate_WaveInForm;
    }
    case ITF_DLG_INIT_CUSTOMIZE_DATA:
    {
        // 使用对话框设置自定义数据
        return (PFN_INTERFACE)emmedia_PropPopDlg_WaveInForm;
    }
    case ITF_NOTIFY_PROPERTY_CHANGED:
    {
        // 通知某属性数据被用户修改
        return (PFN_INTERFACE)emmedia_PropChanged_WaveInForm;
    }
    case ITF_GET_ALL_PROPERTY_DATA:
    {
        // 取全部属性数据
        return (PFN_INTERFACE)emmedia_PropGetDataAll_WaveInForm;
    }
    case ITF_GET_PROPERTY_DATA:
    {
        // 取某属性数据
        return (PFN_INTERFACE)emmedia_PropGetData_WaveInForm;
    }
    case ITF_GET_ICON_PROPERTY_DATA:
    {
        // 取窗口的图标属性数据(仅用于窗口)
        break;
    }
    case ITF_IS_NEED_THIS_KEY:
    {
        // 询问单元是否需要指定的按键信息, 用作窗口单元
        return (PFN_INTERFACE)emmedia_PropKetInfo_WaveInForm;
    }
    case ITF_LANG_CNV:
    {
        // 组件数据语言转换
        break;
    }
    case ITF_MSG_FILTER:
    {
        // 消息过滤
        break;
    }
    case ITF_GET_NOTIFY_RECEIVER:
    {
        // 取组件的附加通知接收者(PFN_ON_NOTIFY_UNIT)
        return (PFN_INTERFACE)emmedia_PropNotifyReceiver_WaveInForm;
    }
    default:
        return NULL;
    }
    return NULL;
}

// 录音音波 下的组件被创建回调, 从易语言IDE选中组件拖放到窗口上触发
HUNIT WINAPI emmedia_ControlCreate_WaveInForm(
    LPBYTE pAllPropertyData,            //   指向本窗口单元的已有属性数据, 由本窗口单元的ITF_GET_PROPERTY_DATA接口产生, 如果没有数据则为NULL
    INT nAllPropertyDataSize,           //   提供pAllPropertyData所指向数据的尺寸, 如果没有则为0
    DWORD dwStyle,                      //   预先设置的窗口风格
    HWND hParentWnd,                    //   父窗口句柄
    UINT uID,                           //   在父窗口中的ID
    HMENU hMenu,                        //   未使用
    INT x, INT y, INT cx, INT cy,       //   指定位置及尺寸
    DWORD dwWinFormID, DWORD dwUnitID,  //   本窗口单元所在窗口及本身的ID, 用作通知到系统
    HWND hDesignWnd,                    //   如果blInDesignMode为真, 则hDesignWnd提供所设计窗口的窗口句柄
    BOOL blInDesignMode                 //   说明是否被易语言IDE调用以进行可视化设计, 运行时为假
)
{
    //TODO 在这里创建组件并返回
    HUNIT hUnit = 0;
    return hUnit;
}

// 录音音波 下的更新属性方法
BOOL WINAPI emmedia_PropUpDate_WaveInForm(HUNIT hUnit, INT nPropertyIndex)
{
    // 如果指定属性目前可以被操作, 返回真, 否则返回假。
    return TRUE;
}

// 录音音波 下的弹出对话框方法
BOOL WINAPI emmedia_PropPopDlg_WaveInForm(HUNIT hUnit, INT nPropertyIndex, BOOL* pblModified, LPVOID pResultExtraData)
{
    // 用作设置类型为UD_CUSTOMIZE的单元属性
    // 如果需要重新创建该单元才能修改单元外形, 请返回真
    // pblModified 不为NULL, 请在其中返回是否被用户真正修改(便于易语言IDE建立UNDO记录)
    *pblModified = false;
    return FALSE;
}

// 录音音波 属性被改变方法
BOOL WINAPI emmedia_PropChanged_WaveInForm(HUNIT hUnit, INT nPropertyIndex,  // 被修改的属性索引
    UNIT_PROPERTY_VALUE* pPropertyVaule, // 用作修改的相应属性数据
    LPTSTR* ppszTipText)    //目前尚未使用
{
    // 通知某属性(非UD_CUSTOMIZE类别属性)数据被用户修改, 需要根据该修改相应更改
    // 内部数据及外形, 如果确实需要重新创建才能修改单元外形, 请返回真
    // 注意：必须进行所传入值的合法性校验
    
    switch (nPropertyIndex)
    {
    case 0: // 这个需要自己修改, 根据索引确定是哪个属性被修改
    {
        break;
    }
    default:
        return false;
    }
    return false;
}

// 录音音波 下的获取全部属性方法
HGLOBAL WINAPI emmedia_PropGetDataAll_WaveInForm(HUNIT hUnit)
{
    // 自己处理一下属性, 返回HGLOBAL 内存句柄, 所有属性信息都写里面
    return 0;
}

// 录音音波 下的获取单个属性方法
BOOL WINAPI emmedia_PropGetData_WaveInForm(HUNIT hUnit, INT nPropertyIndex, PUNIT_PROPERTY_VALUE pPropertyVaule)
{
    // 取某属性数据到pPropertyValue中, 成功返回真, 否则返回假
    // 注意：如果在设计时(由调用PFN_CREATE_UNIT时的blInDesignMode参数决定)
    // pPropertyValue必须返回所存储的值。如果在运行时(blInDesignMode为假)
    // 必须返回实际的当前实时值
    // 比如说, 编辑框窗口单元的"内容"属性, 设计时必须返回内部所保存的值
    // 而运行时就必须调用 GetWindowText 去实时获取
    
    switch (nPropertyIndex)
    {
    case 0: // 这个需要自己修改, 根据索引确定是哪个属性被修改
    {
        break;
    }
    default:
        return false;
    }
    return true;
}

// 录音音波 询问单元是否需要指定的按键信息, 如果需要, 返回真, 否则返回假
BOOL WINAPI emmedia_PropKetInfo_WaveInForm(HUNIT hUnit, WORD wKey)
{
    // 询问单元是否需要指定的按键信息, 如果需要, 返回真, 否则返回假
    return FALSE;
}

// 录音音波 取设计时组件被单击放置到窗体上时的默认创建尺寸
INT WINAPI emmedia_PropNotifyReceiver_WaveInForm(INT nMsg, DWORD dwParam1, DWORD dwParam2)
{
    switch (nMsg)
    {
    case NU_GET_CREATE_SIZE_IN_DESIGNER:
    {
        // 取设计时组件被单击放置到窗体上时的默认创建尺寸
        // dwParam1: 类型: INT*, 返回宽度(单位像素)
        // dwParam2: 类型: INT*, 返回高度(单位像素)
        // 成功返回1,失败返回0.
        // *((int*)dwParam1) = 宽度; // 组件拖放到ide上就使用这个默认宽度
        // *((int*)dwParam2) = 高度; // 组件拖放到ide上就使用这个默认高度
        // return 1; // 使用修改后的宽高, 自己根据需要修改
        break;
    }
    default:
        return 0;
    }
    return 0;
}

// CD播放组件交互方法
EXTERN_C PFN_INTERFACE WINAPI emmedia_GetInterface_CDPlay(INT nInterfaceNO)
{
    switch (nInterfaceNO)
    {
    case ITF_CREATE_UNIT:
    {
        // 创建组件
        return (PFN_INTERFACE)emmedia_ControlCreate_CDPlay;
    }
    case ITF_PROPERTY_UPDATE_UI:
    {
        // 说明属性目前可否被修改
        return (PFN_INTERFACE)emmedia_PropUpDate_CDPlay;
    }
    case ITF_DLG_INIT_CUSTOMIZE_DATA:
    {
        // 使用对话框设置自定义数据
        return (PFN_INTERFACE)emmedia_PropPopDlg_CDPlay;
    }
    case ITF_NOTIFY_PROPERTY_CHANGED:
    {
        // 通知某属性数据被用户修改
        return (PFN_INTERFACE)emmedia_PropChanged_CDPlay;
    }
    case ITF_GET_ALL_PROPERTY_DATA:
    {
        // 取全部属性数据
        return (PFN_INTERFACE)emmedia_PropGetDataAll_CDPlay;
    }
    case ITF_GET_PROPERTY_DATA:
    {
        // 取某属性数据
        return (PFN_INTERFACE)emmedia_PropGetData_CDPlay;
    }
    case ITF_GET_ICON_PROPERTY_DATA:
    {
        // 取窗口的图标属性数据(仅用于窗口)
        break;
    }
    case ITF_IS_NEED_THIS_KEY:
    {
        // 询问单元是否需要指定的按键信息, 用作窗口单元
        return (PFN_INTERFACE)emmedia_PropKetInfo_CDPlay;
    }
    case ITF_LANG_CNV:
    {
        // 组件数据语言转换
        break;
    }
    case ITF_MSG_FILTER:
    {
        // 消息过滤
        break;
    }
    case ITF_GET_NOTIFY_RECEIVER:
    {
        // 取组件的附加通知接收者(PFN_ON_NOTIFY_UNIT)
        return (PFN_INTERFACE)emmedia_PropNotifyReceiver_CDPlay;
    }
    default:
        return NULL;
    }
    return NULL;
}

// CD播放 下的组件被创建回调, 从易语言IDE选中组件拖放到窗口上触发
HUNIT WINAPI emmedia_ControlCreate_CDPlay(
    LPBYTE pAllPropertyData,            //   指向本窗口单元的已有属性数据, 由本窗口单元的ITF_GET_PROPERTY_DATA接口产生, 如果没有数据则为NULL
    INT nAllPropertyDataSize,           //   提供pAllPropertyData所指向数据的尺寸, 如果没有则为0
    DWORD dwStyle,                      //   预先设置的窗口风格
    HWND hParentWnd,                    //   父窗口句柄
    UINT uID,                           //   在父窗口中的ID
    HMENU hMenu,                        //   未使用
    INT x, INT y, INT cx, INT cy,       //   指定位置及尺寸
    DWORD dwWinFormID, DWORD dwUnitID,  //   本窗口单元所在窗口及本身的ID, 用作通知到系统
    HWND hDesignWnd,                    //   如果blInDesignMode为真, 则hDesignWnd提供所设计窗口的窗口句柄
    BOOL blInDesignMode                 //   说明是否被易语言IDE调用以进行可视化设计, 运行时为假
)
{
    //TODO 在这里创建组件并返回
    HUNIT hUnit = 0;
    return hUnit;
}

// CD播放 下的更新属性方法
BOOL WINAPI emmedia_PropUpDate_CDPlay(HUNIT hUnit, INT nPropertyIndex)
{
    // 如果指定属性目前可以被操作, 返回真, 否则返回假。
    return TRUE;
}

// CD播放 下的弹出对话框方法
BOOL WINAPI emmedia_PropPopDlg_CDPlay(HUNIT hUnit, INT nPropertyIndex, BOOL* pblModified, LPVOID pResultExtraData)
{
    // 用作设置类型为UD_CUSTOMIZE的单元属性
    // 如果需要重新创建该单元才能修改单元外形, 请返回真
    // pblModified 不为NULL, 请在其中返回是否被用户真正修改(便于易语言IDE建立UNDO记录)
    *pblModified = false;
    return FALSE;
}

// CD播放 属性被改变方法
BOOL WINAPI emmedia_PropChanged_CDPlay(HUNIT hUnit, INT nPropertyIndex,  // 被修改的属性索引
    UNIT_PROPERTY_VALUE* pPropertyVaule, // 用作修改的相应属性数据
    LPTSTR* ppszTipText)    //目前尚未使用
{
    // 通知某属性(非UD_CUSTOMIZE类别属性)数据被用户修改, 需要根据该修改相应更改
    // 内部数据及外形, 如果确实需要重新创建才能修改单元外形, 请返回真
    // 注意：必须进行所传入值的合法性校验
    
    switch (nPropertyIndex)
    {
    case 0: // 这个需要自己修改, 根据索引确定是哪个属性被修改
    {
        break;
    }
    default:
        return false;
    }
    return false;
}

// CD播放 下的获取全部属性方法
HGLOBAL WINAPI emmedia_PropGetDataAll_CDPlay(HUNIT hUnit)
{
    // 自己处理一下属性, 返回HGLOBAL 内存句柄, 所有属性信息都写里面
    return 0;
}

// CD播放 下的获取单个属性方法
BOOL WINAPI emmedia_PropGetData_CDPlay(HUNIT hUnit, INT nPropertyIndex, PUNIT_PROPERTY_VALUE pPropertyVaule)
{
    // 取某属性数据到pPropertyValue中, 成功返回真, 否则返回假
    // 注意：如果在设计时(由调用PFN_CREATE_UNIT时的blInDesignMode参数决定)
    // pPropertyValue必须返回所存储的值。如果在运行时(blInDesignMode为假)
    // 必须返回实际的当前实时值
    // 比如说, 编辑框窗口单元的"内容"属性, 设计时必须返回内部所保存的值
    // 而运行时就必须调用 GetWindowText 去实时获取
    
    switch (nPropertyIndex)
    {
    case 0: // 这个需要自己修改, 根据索引确定是哪个属性被修改
    {
        break;
    }
    default:
        return false;
    }
    return true;
}

// CD播放 询问单元是否需要指定的按键信息, 如果需要, 返回真, 否则返回假
BOOL WINAPI emmedia_PropKetInfo_CDPlay(HUNIT hUnit, WORD wKey)
{
    // 询问单元是否需要指定的按键信息, 如果需要, 返回真, 否则返回假
    return FALSE;
}

// CD播放 取设计时组件被单击放置到窗体上时的默认创建尺寸
INT WINAPI emmedia_PropNotifyReceiver_CDPlay(INT nMsg, DWORD dwParam1, DWORD dwParam2)
{
    switch (nMsg)
    {
    case NU_GET_CREATE_SIZE_IN_DESIGNER:
    {
        // 取设计时组件被单击放置到窗体上时的默认创建尺寸
        // dwParam1: 类型: INT*, 返回宽度(单位像素)
        // dwParam2: 类型: INT*, 返回高度(单位像素)
        // 成功返回1,失败返回0.
        // *((int*)dwParam1) = 宽度; // 组件拖放到ide上就使用这个默认宽度
        // *((int*)dwParam2) = 高度; // 组件拖放到ide上就使用这个默认高度
        // return 1; // 使用修改后的宽高, 自己根据需要修改
        break;
    }
    default:
        return 0;
    }
    return 0;
}

// 媒体播放组件交互方法
EXTERN_C PFN_INTERFACE WINAPI emmedia_GetInterface_MediaPlay(INT nInterfaceNO)
{
    switch (nInterfaceNO)
    {
    case ITF_CREATE_UNIT:
    {
        // 创建组件
        return (PFN_INTERFACE)emmedia_ControlCreate_MediaPlay;
    }
    case ITF_PROPERTY_UPDATE_UI:
    {
        // 说明属性目前可否被修改
        return (PFN_INTERFACE)emmedia_PropUpDate_MediaPlay;
    }
    case ITF_DLG_INIT_CUSTOMIZE_DATA:
    {
        // 使用对话框设置自定义数据
        return (PFN_INTERFACE)emmedia_PropPopDlg_MediaPlay;
    }
    case ITF_NOTIFY_PROPERTY_CHANGED:
    {
        // 通知某属性数据被用户修改
        return (PFN_INTERFACE)emmedia_PropChanged_MediaPlay;
    }
    case ITF_GET_ALL_PROPERTY_DATA:
    {
        // 取全部属性数据
        return (PFN_INTERFACE)emmedia_PropGetDataAll_MediaPlay;
    }
    case ITF_GET_PROPERTY_DATA:
    {
        // 取某属性数据
        return (PFN_INTERFACE)emmedia_PropGetData_MediaPlay;
    }
    case ITF_GET_ICON_PROPERTY_DATA:
    {
        // 取窗口的图标属性数据(仅用于窗口)
        break;
    }
    case ITF_IS_NEED_THIS_KEY:
    {
        // 询问单元是否需要指定的按键信息, 用作窗口单元
        return (PFN_INTERFACE)emmedia_PropKetInfo_MediaPlay;
    }
    case ITF_LANG_CNV:
    {
        // 组件数据语言转换
        break;
    }
    case ITF_MSG_FILTER:
    {
        // 消息过滤
        break;
    }
    case ITF_GET_NOTIFY_RECEIVER:
    {
        // 取组件的附加通知接收者(PFN_ON_NOTIFY_UNIT)
        return (PFN_INTERFACE)emmedia_PropNotifyReceiver_MediaPlay;
    }
    default:
        return NULL;
    }
    return NULL;
}

// 媒体播放 下的组件被创建回调, 从易语言IDE选中组件拖放到窗口上触发
HUNIT WINAPI emmedia_ControlCreate_MediaPlay(
    LPBYTE pAllPropertyData,            //   指向本窗口单元的已有属性数据, 由本窗口单元的ITF_GET_PROPERTY_DATA接口产生, 如果没有数据则为NULL
    INT nAllPropertyDataSize,           //   提供pAllPropertyData所指向数据的尺寸, 如果没有则为0
    DWORD dwStyle,                      //   预先设置的窗口风格
    HWND hParentWnd,                    //   父窗口句柄
    UINT uID,                           //   在父窗口中的ID
    HMENU hMenu,                        //   未使用
    INT x, INT y, INT cx, INT cy,       //   指定位置及尺寸
    DWORD dwWinFormID, DWORD dwUnitID,  //   本窗口单元所在窗口及本身的ID, 用作通知到系统
    HWND hDesignWnd,                    //   如果blInDesignMode为真, 则hDesignWnd提供所设计窗口的窗口句柄
    BOOL blInDesignMode                 //   说明是否被易语言IDE调用以进行可视化设计, 运行时为假
)
{
    //TODO 在这里创建组件并返回
    HUNIT hUnit = 0;
    return hUnit;
}

// 媒体播放 下的更新属性方法
BOOL WINAPI emmedia_PropUpDate_MediaPlay(HUNIT hUnit, INT nPropertyIndex)
{
    // 如果指定属性目前可以被操作, 返回真, 否则返回假。
    return TRUE;
}

// 媒体播放 下的弹出对话框方法
BOOL WINAPI emmedia_PropPopDlg_MediaPlay(HUNIT hUnit, INT nPropertyIndex, BOOL* pblModified, LPVOID pResultExtraData)
{
    // 用作设置类型为UD_CUSTOMIZE的单元属性
    // 如果需要重新创建该单元才能修改单元外形, 请返回真
    // pblModified 不为NULL, 请在其中返回是否被用户真正修改(便于易语言IDE建立UNDO记录)
    *pblModified = false;
    return FALSE;
}

// 媒体播放 属性被改变方法
BOOL WINAPI emmedia_PropChanged_MediaPlay(HUNIT hUnit, INT nPropertyIndex,  // 被修改的属性索引
    UNIT_PROPERTY_VALUE* pPropertyVaule, // 用作修改的相应属性数据
    LPTSTR* ppszTipText)    //目前尚未使用
{
    // 通知某属性(非UD_CUSTOMIZE类别属性)数据被用户修改, 需要根据该修改相应更改
    // 内部数据及外形, 如果确实需要重新创建才能修改单元外形, 请返回真
    // 注意：必须进行所传入值的合法性校验
    
    switch (nPropertyIndex)
    {
    case 0: // 这个需要自己修改, 根据索引确定是哪个属性被修改
    {
        break;
    }
    default:
        return false;
    }
    return false;
}

// 媒体播放 下的获取全部属性方法
HGLOBAL WINAPI emmedia_PropGetDataAll_MediaPlay(HUNIT hUnit)
{
    // 自己处理一下属性, 返回HGLOBAL 内存句柄, 所有属性信息都写里面
    return 0;
}

// 媒体播放 下的获取单个属性方法
BOOL WINAPI emmedia_PropGetData_MediaPlay(HUNIT hUnit, INT nPropertyIndex, PUNIT_PROPERTY_VALUE pPropertyVaule)
{
    // 取某属性数据到pPropertyValue中, 成功返回真, 否则返回假
    // 注意：如果在设计时(由调用PFN_CREATE_UNIT时的blInDesignMode参数决定)
    // pPropertyValue必须返回所存储的值。如果在运行时(blInDesignMode为假)
    // 必须返回实际的当前实时值
    // 比如说, 编辑框窗口单元的"内容"属性, 设计时必须返回内部所保存的值
    // 而运行时就必须调用 GetWindowText 去实时获取
    
    switch (nPropertyIndex)
    {
    case 0: // 这个需要自己修改, 根据索引确定是哪个属性被修改
    {
        break;
    }
    default:
        return false;
    }
    return true;
}

// 媒体播放 询问单元是否需要指定的按键信息, 如果需要, 返回真, 否则返回假
BOOL WINAPI emmedia_PropKetInfo_MediaPlay(HUNIT hUnit, WORD wKey)
{
    // 询问单元是否需要指定的按键信息, 如果需要, 返回真, 否则返回假
    return FALSE;
}

// 媒体播放 取设计时组件被单击放置到窗体上时的默认创建尺寸
INT WINAPI emmedia_PropNotifyReceiver_MediaPlay(INT nMsg, DWORD dwParam1, DWORD dwParam2)
{
    switch (nMsg)
    {
    case NU_GET_CREATE_SIZE_IN_DESIGNER:
    {
        // 取设计时组件被单击放置到窗体上时的默认创建尺寸
        // dwParam1: 类型: INT*, 返回宽度(单位像素)
        // dwParam2: 类型: INT*, 返回高度(单位像素)
        // 成功返回1,失败返回0.
        // *((int*)dwParam1) = 宽度; // 组件拖放到ide上就使用这个默认宽度
        // *((int*)dwParam2) = 高度; // 组件拖放到ide上就使用这个默认高度
        // return 1; // 使用修改后的宽高, 自己根据需要修改
        break;
    }
    default:
        return 0;
    }
    return 0;
}

// 音频转换组件交互方法
EXTERN_C PFN_INTERFACE WINAPI emmedia_GetInterface_AudioConvert(INT nInterfaceNO)
{
    switch (nInterfaceNO)
    {
    case ITF_CREATE_UNIT:
    {
        // 创建组件
        return (PFN_INTERFACE)emmedia_ControlCreate_AudioConvert;
    }
    case ITF_PROPERTY_UPDATE_UI:
    {
        // 说明属性目前可否被修改
        return (PFN_INTERFACE)emmedia_PropUpDate_AudioConvert;
    }
    case ITF_DLG_INIT_CUSTOMIZE_DATA:
    {
        // 使用对话框设置自定义数据
        return (PFN_INTERFACE)emmedia_PropPopDlg_AudioConvert;
    }
    case ITF_NOTIFY_PROPERTY_CHANGED:
    {
        // 通知某属性数据被用户修改
        return (PFN_INTERFACE)emmedia_PropChanged_AudioConvert;
    }
    case ITF_GET_ALL_PROPERTY_DATA:
    {
        // 取全部属性数据
        return (PFN_INTERFACE)emmedia_PropGetDataAll_AudioConvert;
    }
    case ITF_GET_PROPERTY_DATA:
    {
        // 取某属性数据
        return (PFN_INTERFACE)emmedia_PropGetData_AudioConvert;
    }
    case ITF_GET_ICON_PROPERTY_DATA:
    {
        // 取窗口的图标属性数据(仅用于窗口)
        break;
    }
    case ITF_IS_NEED_THIS_KEY:
    {
        // 询问单元是否需要指定的按键信息, 用作窗口单元
        return (PFN_INTERFACE)emmedia_PropKetInfo_AudioConvert;
    }
    case ITF_LANG_CNV:
    {
        // 组件数据语言转换
        break;
    }
    case ITF_MSG_FILTER:
    {
        // 消息过滤
        break;
    }
    case ITF_GET_NOTIFY_RECEIVER:
    {
        // 取组件的附加通知接收者(PFN_ON_NOTIFY_UNIT)
        return (PFN_INTERFACE)emmedia_PropNotifyReceiver_AudioConvert;
    }
    default:
        return NULL;
    }
    return NULL;
}

// 音频转换 下的组件被创建回调, 从易语言IDE选中组件拖放到窗口上触发
HUNIT WINAPI emmedia_ControlCreate_AudioConvert(
    LPBYTE pAllPropertyData,            //   指向本窗口单元的已有属性数据, 由本窗口单元的ITF_GET_PROPERTY_DATA接口产生, 如果没有数据则为NULL
    INT nAllPropertyDataSize,           //   提供pAllPropertyData所指向数据的尺寸, 如果没有则为0
    DWORD dwStyle,                      //   预先设置的窗口风格
    HWND hParentWnd,                    //   父窗口句柄
    UINT uID,                           //   在父窗口中的ID
    HMENU hMenu,                        //   未使用
    INT x, INT y, INT cx, INT cy,       //   指定位置及尺寸
    DWORD dwWinFormID, DWORD dwUnitID,  //   本窗口单元所在窗口及本身的ID, 用作通知到系统
    HWND hDesignWnd,                    //   如果blInDesignMode为真, 则hDesignWnd提供所设计窗口的窗口句柄
    BOOL blInDesignMode                 //   说明是否被易语言IDE调用以进行可视化设计, 运行时为假
)
{
    //TODO 在这里创建组件并返回
    HUNIT hUnit = 0;
    return hUnit;
}

// 音频转换 下的更新属性方法
BOOL WINAPI emmedia_PropUpDate_AudioConvert(HUNIT hUnit, INT nPropertyIndex)
{
    // 如果指定属性目前可以被操作, 返回真, 否则返回假。
    return TRUE;
}

// 音频转换 下的弹出对话框方法
BOOL WINAPI emmedia_PropPopDlg_AudioConvert(HUNIT hUnit, INT nPropertyIndex, BOOL* pblModified, LPVOID pResultExtraData)
{
    // 用作设置类型为UD_CUSTOMIZE的单元属性
    // 如果需要重新创建该单元才能修改单元外形, 请返回真
    // pblModified 不为NULL, 请在其中返回是否被用户真正修改(便于易语言IDE建立UNDO记录)
    *pblModified = false;
    return FALSE;
}

// 音频转换 属性被改变方法
BOOL WINAPI emmedia_PropChanged_AudioConvert(HUNIT hUnit, INT nPropertyIndex,  // 被修改的属性索引
    UNIT_PROPERTY_VALUE* pPropertyVaule, // 用作修改的相应属性数据
    LPTSTR* ppszTipText)    //目前尚未使用
{
    // 通知某属性(非UD_CUSTOMIZE类别属性)数据被用户修改, 需要根据该修改相应更改
    // 内部数据及外形, 如果确实需要重新创建才能修改单元外形, 请返回真
    // 注意：必须进行所传入值的合法性校验
    
    switch (nPropertyIndex)
    {
    case 0: // 这个需要自己修改, 根据索引确定是哪个属性被修改
    {
        break;
    }
    default:
        return false;
    }
    return false;
}

// 音频转换 下的获取全部属性方法
HGLOBAL WINAPI emmedia_PropGetDataAll_AudioConvert(HUNIT hUnit)
{
    // 自己处理一下属性, 返回HGLOBAL 内存句柄, 所有属性信息都写里面
    return 0;
}

// 音频转换 下的获取单个属性方法
BOOL WINAPI emmedia_PropGetData_AudioConvert(HUNIT hUnit, INT nPropertyIndex, PUNIT_PROPERTY_VALUE pPropertyVaule)
{
    // 取某属性数据到pPropertyValue中, 成功返回真, 否则返回假
    // 注意：如果在设计时(由调用PFN_CREATE_UNIT时的blInDesignMode参数决定)
    // pPropertyValue必须返回所存储的值。如果在运行时(blInDesignMode为假)
    // 必须返回实际的当前实时值
    // 比如说, 编辑框窗口单元的"内容"属性, 设计时必须返回内部所保存的值
    // 而运行时就必须调用 GetWindowText 去实时获取
    
    switch (nPropertyIndex)
    {
    case 0: // 这个需要自己修改, 根据索引确定是哪个属性被修改
    {
        break;
    }
    default:
        return false;
    }
    return true;
}

// 音频转换 询问单元是否需要指定的按键信息, 如果需要, 返回真, 否则返回假
BOOL WINAPI emmedia_PropKetInfo_AudioConvert(HUNIT hUnit, WORD wKey)
{
    // 询问单元是否需要指定的按键信息, 如果需要, 返回真, 否则返回假
    return FALSE;
}

// 音频转换 取设计时组件被单击放置到窗体上时的默认创建尺寸
INT WINAPI emmedia_PropNotifyReceiver_AudioConvert(INT nMsg, DWORD dwParam1, DWORD dwParam2)
{
    switch (nMsg)
    {
    case NU_GET_CREATE_SIZE_IN_DESIGNER:
    {
        // 取设计时组件被单击放置到窗体上时的默认创建尺寸
        // dwParam1: 类型: INT*, 返回宽度(单位像素)
        // dwParam2: 类型: INT*, 返回高度(单位像素)
        // 成功返回1,失败返回0.
        // *((int*)dwParam1) = 宽度; // 组件拖放到ide上就使用这个默认宽度
        // *((int*)dwParam2) = 高度; // 组件拖放到ide上就使用这个默认高度
        // return 1; // 使用修改后的宽高, 自己根据需要修改
        break;
    }
    default:
        return 0;
    }
    return 0;
}



